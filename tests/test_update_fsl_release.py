#!/usr/bin/env python
#
# test_update_fsl_release.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import contextlib
import os.path as op
import os
import json
import yaml
import shlex

from unittest import mock

import fsl.installer               as fi
import fsl.base.conda              as conda
import fsl.base.update_fsl_release as update_fsl_release


# IMPORTANT: All stated versions of all packages must actually exist


def query_installed_packages(*args, **kwargs):
    conda.query_installed_packages.cache_clear()
    return conda.query_installed_packages(*args, **kwargs)


def create_dummy_manifest_environment_files(installed, available):

    plat         = fi.identify_platform()
    envfile      = op.abspath(f'fsl-{available}_{plat}.yml')
    manifestfile = 'manifest.json'
    env          = {
        'name' : 'FSL',
        'channels' : [
            'https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/',
            'conda-forge'],
        'dependencies' : [
            'python 3.10.*',
            'fslpy 3.10.0',
            'fsl-base 2301.1',
            'fsl-installer 3.0.0']
    }

    with open(envfile, 'wt') as f:
        f.write(yaml.dump(env, Dumper=yaml.Dumper))

    manifest = {
        'versions' : {
            'latest'   : available,
            available  : [
                {
                    'platform'      : plat,
                    'environment'   : envfile,
                    'sha256'        : fi.sha256(envfile),
                    'base_packages' : ['fsl-base', 'fslpy'],
                    'output'        : {
                        'install'   : '92',
                        installed   : '11'
                    }
                },
            ]
        }
    }

    with open(manifestfile, 'wt') as f:
        f.write(json.dumps(manifest))


@contextlib.contextmanager
def create_test_fsldir(fslversion):
    with fi.tempdir() as td:
        condabin = conda.conda()
        fsldir   = op.join(td, 'fsl')

        # older versions than in the
        # dummy env file created above
        packages = [
            'python=3.10',
            'fslpy=3.9.5',
            'fsl-base=2211.1',
            'fsl-installer=2.1.0']
        channels = [
            conda.PUBLIC_FSL_CHANNEL,
            'conda-forge']
        cmd = f'{condabin} create -y -p {fsldir} '            + \
            ' '.join(f'-c {chan}' for chan in channels) + ' ' + \
            ' '.join(packages)

        fi.Process.check_call(cmd, print_output=True)
        with open(op.join(fsldir, 'etc', 'fslversion'), 'wt') as f:
            f.write(fslversion)

        condarc = op.join(fsldir, '.condarc')
        with open(condarc, 'wt') as f:
            f.write('channels:\n')
            for c in channels:
                f.write(f' - {c}\n')

        # mamba only honours $PREFIX/.condarc
        # if the environment is activated, so
        # we use $CONDARC. The clean_environ
        # function clears all FSL/CONDA env
        # vars, so we patch that function too.
        with mock.patch.dict('os.environ', FSLDIR=fsldir, CONDARC=condarc), \
             mock.patch('fsl.installer.fslinstaller.clean_environ', return_value=os.environ):
            yield fsldir


def test_update_fsl_release():
    installed = '5.0.0'
    available = '7.0.0'
    with create_test_fsldir(installed) as fsldir:
        create_dummy_manifest_environment_files(installed, available)

        pkgs = query_installed_packages(fsldir, True)
        assert pkgs['fslpy']        .version == '3.9.5'
        assert pkgs['fsl-base']     .version == '2211.1'
        assert pkgs['fsl-installer'].version == '2.1.0'

        try:
            update_fsl_release.main(shlex.split(
                '--yes --manifest=manifest.json --logfile log.txt'))
        finally:
            if op.exists('log.txt'):
                with open('log.txt', 'rt') as f:
                    print(f.read())

        pkgs = query_installed_packages(fsldir, True)
        assert pkgs['fslpy']        .version == '3.10.0'
        assert pkgs['fsl-base']     .version == '2301.1'
        assert pkgs['fsl-installer'].version == '3.0.0'


def test_update_fsl_release_already_up_to_date():
    installed = '5.0.0'
    available = '5.0.0'
    with create_test_fsldir(installed) as fsldir:
        create_dummy_manifest_environment_files(installed, available)

        pkgs = query_installed_packages(fsldir, True)
        assert pkgs['fslpy']        .version == '3.9.5'
        assert pkgs['fsl-base']     .version == '2211.1'
        assert pkgs['fsl-installer'].version == '2.1.0'

        update_fsl_release.main(shlex.split('--yes --manifest=manifest.json'))

        pkgs = query_installed_packages(fsldir, True)
        assert pkgs['fslpy']        .version == '3.9.5'
        assert pkgs['fsl-base']     .version == '2211.1'
        assert pkgs['fsl-installer'].version == '2.1.0'
