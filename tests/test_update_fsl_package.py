#!/usr/bin/env python


import os
import os.path as op
import shlex
import subprocess as sp
import sys
import tempfile

from unittest import mock


import fsl.installer               as fi
import fsl.base.update_fsl_package as update_fsl_package
import fsl.base.conda              as conda


def run(fsldir, cmd):
    exitcode = []
    def sysexit(code=None):
        exitcode.append(code)
    print(f'Running: update_fsl_package {cmd}')
    conda.download_package_metadata.cache_clear()
    conda.download_channel_metadata.cache_clear()
    conda.query_installed_packages .cache_clear()
    with mock.patch(     'sys.exit',   sysexit), \
         mock.patch.dict('os.environ', FSLDIR=fsldir):
        update_fsl_package.main(shlex.split(cmd))
    assert len(exitcode) == 0 or (len(exitcode) == 1 and exitcode[0] in (None, 0))


def create_test_env(envdir):
    # install slightly older versions of
    # the fslpy/fsl-installer packages
    pyver        = '3.10'
    fslpyver     = '3.9'
    installerver = '2.1.0'
    condabin     = conda.conda()
    fi.Process.check_call(
        f'{condabin} create -y -p {envdir} '
        f'-c {conda.PUBLIC_FSL_CHANNEL} -c conda-forge '
        f'python={pyver} fslpy={fslpyver} fsl-installer={installerver}',
        print_output=True)


def test_update_fsl_package():
    # do some dry runs against this FSL installation
    fsldir = os.environ['FSLDIR']
    run(fsldir, f'-y -a -d -e --verbose --dry_run')
    run(fsldir, f'-y -a -d    --verbose --dry_run')
    run(fsldir, f'-y -a    -e --verbose --dry_run')
    run(fsldir, f'-y -a       --verbose --dry_run')

    # do some actual installs against a dummy FSLDIR
    with tempfile.TemporaryDirectory() as parentdir:
        fsldir = op.join(parentdir, 'fsl')
        create_test_env(fsldir)
        run(fsldir, '-y -a -e -d --verbose')
