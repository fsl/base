#!/usr/bin/env python
#
# test_fslversion.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import shlex
import tempfile

import fsl.base.fslversion as fslver


def test_fslversion():
    fslver.main([])
    fslver.main(['-v',])
    fslver.main(['-p',])
    with tempfile.TemporaryDirectory() as parentdir:
        fslver.main(['-f', 'env.yml'])
        with open('env.yml', 'rt') as f:
            print(f.read())
