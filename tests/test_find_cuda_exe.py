#!/usr/bin/env python

import contextlib
import os
import sys
import os.path as op
import textwrap as tw

import tempfile
import subprocess as sp

from unittest import mock

import fsl.base.find_cuda_exe as find_cuda_exe


def call_find_cuda_exe(prefix):
    calls = []
    def mock_print(s):
        calls.append(s)
    def sysexit(code=None):
        pass
    with mock.patch('builtins.print', mock_print), \
         mock.patch('sys.exit',       sysexit):
        find_cuda_exe.main((prefix,))
    return '\n'.join(calls).strip()


@contextlib.contextmanager
def mock_nvidia_smi(version=9.2):
    with tempfile.TemporaryDirectory() as td:
        script = tw.dedent("""
        #!/usr/bin/env bash

        echo CUDA Version: {:.1f}
        """.format(version)).strip()

        smipath = op.join(td, 'nvidia-smi')

        with open(smipath, 'wt') as f:
            f.write(script)
        os.chmod(smipath, 0o777)

        path = op.pathsep.join((td, os.environ['PATH']))

        with mock.patch.dict('os.environ', {'PATH' : path}):
            yield


@contextlib.contextmanager
def mock_fsldir(prefix, *cudavers, fsldevdir=False):
    with tempfile.TemporaryDirectory() as fsldir:

        bindir = op.join(fsldir, 'bin')
        os.makedirs(bindir)

        # Files called $FSLDIR/bin/<prefix> should
        # not be picked up by the find_cuda_exe script.
        with open(op.join(bindir, prefix), 'wt') as f:
            pass

        for cudaver in cudavers:
            with open(op.join(bindir, f'{prefix}{cudaver:.1f}'), 'wt') as f:
                pass

        if fsldevdir: env = {'FSLDEVDIR' : fsldir}
        else:         env = {'FSLDIR'    : fsldir}

        with mock.patch.dict('os.environ', env):
            yield fsldir

def test_find_cuda_exe_no_cuda():
    # Assuming here that nvidia-smi is not on the $PATH
    assert call_find_cuda_exe('prefix') == ''


def test_find_cuda_exe_no_exes():
    # nvidia-smi present, but no $FSLDIR/bin/prefixX.Y exes
    with mock_nvidia_smi():
        assert call_find_cuda_exe('prefix') == ''


def test_find_cuda_exe():
    with mock_nvidia_smi(9.2), mock_fsldir('exe_prefix', 8.0, 9.0) as fsldir:
        expect = op.join(fsldir, 'bin', 'exe_prefix9.0')
        assert call_find_cuda_exe('exe_prefix') == expect

    with mock_nvidia_smi(9.2), mock_fsldir('exe_prefix', 8.0, 9.0, 10.0) as fsldir:
        expect = op.join(fsldir, 'bin', 'exe_prefix9.0')
        assert call_find_cuda_exe('exe_prefix') == expect

    with mock_nvidia_smi(9.2), mock_fsldir('exe_prefix', 8.0, 9.2, 10.0) as fsldir:
        expect = op.join(fsldir, 'bin', 'exe_prefix9.2')
        assert call_find_cuda_exe('exe_prefix') == expect

    # nvidia-smi reporting an older CUDA -
    # just return the oldest available exe,
    # as the driver might support newer
    # CUDA versions than what is reported.
    with mock_nvidia_smi(9.2), mock_fsldir('exe_prefix', 10.0, 11.0, 12.0) as fsldir:
        expect = op.join(fsldir, 'bin', 'exe_prefix10.0')
        print(call_find_cuda_exe('exe_prefix'))
        assert call_find_cuda_exe('exe_prefix') == expect


def test_find_cuda_exe_fsldevdir():
    with mock_nvidia_smi(10.2),                                   \
         mock_fsldir('exe_prefix', 8.0, 9.0)           as fsldir, \
         mock_fsldir('exe_prefix', 10, fsldevdir=True) as fsldevdir:
        expect = op.join(fsldevdir, 'bin', 'exe_prefix10.0')
        assert call_find_cuda_exe('exe_prefix') == expect
