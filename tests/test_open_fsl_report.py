#!/usr/bin/env python
#
# test_open_fsl_report.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os.path as op
import tempfile
from unittest import mock

import fsl.base.open_fsl_report as open_fsl_report

htmlfile = """
<html>
<body>
<h1>hi</h1>
</body>
</html>
""".strip()

def test_open_fsl_report():

    with tempfile.TemporaryDirectory() as td, \
        mock.patch('webbrowser.open_new_tab', return_value=True):

        filepath = op.join(td, 'file.html')

        with open(filepath, 'wt') as f:
            f.write(htmlfile)
        open_fsl_report.main([filepath, '-s', '2'])
