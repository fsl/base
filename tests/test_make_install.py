#!/usr/bin/env python

from   contextlib import contextmanager
import os
import os.path as op
import shlex
import subprocess as sp
import sys
import tempfile
import textwrap as tw
from   unittest import mock


@contextmanager
def mock_uname(platform):
    uname = tw.dedent(f"""
    #!/usr/bin/env bash
    echo {platform}
    """).strip()
    with tempfile.TemporaryDirectory() as td:
        fpath = op.join(td, 'uname')
        with open(fpath, 'wt') as f:
            f.write(uname)
        os.chmod(fpath, 0o755)
        path = op.pathsep.join((td, os.environ['PATH']))

        # the FSL makefile rules use $SYSTYPE to
        # store the platform id ("Linux" or "Darwin")
        with mock.patch.dict(os.environ, PATH=path, SYSTYPE=platform):
            yield fpath


def touch(path, contents=''):
    dirname = op.dirname(path)
    if not op.exists(dirname):
        os.makedirs(dirname)
    with open(path, 'wt') as f:
        f.write(contents)


# sanity check that "make install" for
# the base project succeeds
def test_make_install():
    basedir = op.join(op.dirname(__file__), '..')
    env = os.environ.copy()
    with tempfile.TemporaryDirectory() as td:

        env['FSLDEVDIR'] = td
        cmd = f'make -C "{basedir}" install'

        print(f'running FSLDEVDIR={td} {cmd}')

        sp.run(shlex.split(cmd), env=env, check=True)

        for dirpath, dirnames, filenames in os.walk(td):
            dirpath = dirpath.replace(td, '${FSLDEVDIR}')
            print(dirpath)
            for filename in filenames:
                print('  ', filename)



# check that GUI links ($FSLDIR/bin/<Tool> / $FSLDIR/bin/<Tool>_gui)
# are created correctly
def test_make_install_gui_links():
    makefile = tw.dedent("""
    include    ${FSLCONFDIR}/default.mk
    PROJNAME = some_tool
    RUNTCLS  = Sometool
    SCRIPTS  = sometool
    """)

    # platform : expected executables
    tests = {
        'Linux'  : ['sometool', 'Sometool_gui', 'Sometool'],
        'Darwin' : ['sometool', 'Sometool_gui']
    }

    # We can't test linux installations on macOS,
    # due to the case-insensitive file system
    if sys.platform.lower() == 'darwin':
        tests.pop('Linux')

    for plat, expected in tests.items():
        with mock_uname(plat),                           \
             tempfile.TemporaryDirectory() as fsldevdir, \
             tempfile.TemporaryDirectory() as srcdir:

            env = os.environ.copy()

            env['FSLDEVDIR'] = fsldevdir
            env['FSLCONFDIR'] = op.join(op.dirname(__file__), '..', 'config')

            with open(op.join(srcdir, 'Makefile'), 'wt') as f:
                f.write(makefile)
            touch(op.join(srcdir, 'sometool'), 'sometool')

            cmd = f'make -C "{srcdir}" install'
            sp.run(shlex.split(cmd), env=env, check=True)

            fslbindir = op.join(fsldevdir, 'bin')

            for exp in expected:
                assert op.lexists(op.join(fslbindir, exp))

            # check that the lower-case "sometool"
            # script was installed correctly
            with open(op.join(fslbindir, 'sometool'), 'rt') as f:
                assert f.read().strip() == 'sometool'
