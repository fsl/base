#!/usr/bin/env python

"""This module contains functions, used by the update_fsl_package and
update_fsl_release scripts, for interacting with conda.
"""


import                        bisect
import                        dataclasses
from   collections     import defaultdict
import functools       as     ft
import                        glob
import                        json
import                        logging
import os.path         as     op
import                        os
import                        re
import                        shutil
import urllib.parse    as     urlparse
import urllib.request  as     urlrequest


from typing import Dict, List, Any, Optional, Tuple, Sequence, Union

import yaml

from fsl.installer import (identify_platform,
                           generate_condarc,
                           Process,
                           Version)


log = logging.getLogger(__name__)


CHANNEL_BASE            = 'https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda'
PUBLIC_FSL_CHANNEL      = f'{CHANNEL_BASE}/public/'
DEVELOPMENT_FSL_CHANNEL = f'{CHANNEL_BASE}/development/'
INTERNAL_FSL_CHANNEL    = f'{CHANNEL_BASE}/internal/'

EXTERNALLY_HOSTED_PACKAGES = {
    'fslpy'           : 'conda-forge',
    'fsleyes-props'   : 'conda-forge',
    'fsleyes-widgets' : 'conda-forge',
    'fsleyes'         : 'conda-forge',
    'fmrib-unpack'    : 'conda-forge',
    'file-tree'       : 'conda-forge',
    'file-tree-fsl'   : 'conda-forge',
    'spec2nii'        : 'conda-forge',
}
"""List of packages which are considered to be part of FSL, but which are
hosted on an external channel (most likely conda-forge). These packages should
still be considered for updates.

The values in this dictionary can either be a channel name on
https://anaconda.org, or a fully qualified channel URL.
"""


def conda():
    """Returns the path to the micromamba/mamba/conda command. """

    fsldir     = os.environ['FSLDIR']
    candidates = [
        op.join(fsldir, 'bin',      'micromamba'),
        op.join(fsldir, 'bin',      'mamba'),
        op.join(fsldir, 'bin',      'conda'),
        op.join(fsldir, 'condabin', 'conda'),
        shutil.which('micromamba'),
        shutil.which('mamba'),
        shutil.which('conda')
    ]

    for condabin in candidates:
        if condabin is None:
            continue
        if op.exists(condabin):
            return condabin

    # If FSLDIR is a child env, we may not be
    # able to find the conda executable. In
    # this case, we have to assume that
    # "conda" is defined as a shell function.
    return 'conda'


def conda_platform_identifier():
    """Returns the conda identifier for this platform, one of "linux-64",
    "osx-64", or "osx-arm64". Note that these identifiers are different
    to those used for FSL releases, which are "linux-64", "macos-64" and
    "macos-M1".
    """
    # convert FSL platform identifier to conda platform idenfitier
    platforms = {'linux-64' : 'linux-64',
                 'macos-64' : 'osx-64',
                 'macos-M1' : 'osx-arm64'}
    return platforms[identify_platform()]


@ft.total_ordering
@dataclasses.dataclass
class Package:
    """Represents a single package file hosted on a conda channel.

    A package object corresponds to a specific version of a specific package,
    for a specific platform.
    """

    name : str
    """Package name."""

    version : str
    """Package version string."""

    build : str
    """Conda build number. """

    buildstr : str
    """Conda build string. """

    channel : str
    """URL of the channel which hosts the package."""

    platform : str
    """Platform identifier."""

    dependencies : List[str] = None
    """References to all packages which this package depends on. Stored as
    "package[ version-constraint]" strings.
    """


    def __lt__(self, pkg):
        """Only valid when comparing another Package with the same name and
        platform.
        """
        return ((Version(self.version) <  Version(pkg.version)) or
                (Version(self.version) == Version(pkg.version) and
                 int(self.build)       <  int(pkg.build)))


    def __eq__(self, pkg):
        """Only valid when comparing another Package with the same name and
        platform.
        """
        return (Version(self.version) == Version(pkg.version) and
                int(self.build)       == int(pkg.build))


def http_request(url      : str,
                 username : str = None,
                 password : str = None) -> Any:
    """Download JSON data from the given URL. """

    if username is not None:
        urlbase = urlparse.urlparse(url).netloc
        pwdmgr = urlrequest.HTTPPasswordMgrWithDefaultRealm()
        pwdmgr.add_password(None, urlbase, username, password)
        handler = urlrequest.HTTPBasicAuthHandler(pwdmgr)
        opener = urlrequest.build_opener(handler)
        opener.open(url)
        urlrequest.install_opener(opener)

    log.debug(f'Downloading {url} ...')

    request = urlrequest.Request(url, method='GET')
    with urlrequest.urlopen(request) as response:
        payload = response.read()

    if len(payload) == 0: payload = {}
    else:                 payload = json.loads(payload)

    return payload


@ft.lru_cache
def download_package_metadata(pkgname : str,
                              channel : str) -> Optional[Package]:
    """Downloads metadata about one externally hosted package. The returned
    Package object does not contain information about dependencies.

    Returns None if the package does not appear to be hosted on the channel.

    pkgname:     Name of package to lookup
    channel:     Name of channel on anaconda.org. Can also be a full channel
                 URL
    """

    # if we've been given a full url, we
    # download the full channel metdata
    # and look up the package. This is
    # expensive if downloading from
    # anaconda.org
    if any(channel.startswith(p) for p in ('https:', 'http:', 'file:')):
        chandata = download_channel_metadata(channel)
        pkgs     = parse_channel_metadata([chandata], [pkgname])
        return pkgs.get(pkgname, [None])[-1]

    # Otherwise channel_url is the name of
    # an anaconda.org channel - we just
    # retrieve information about the one
    # package using the HTTP API.
    #
    # https://api.anaconda.org/docs
    api_url     = f'https://api.anaconda.org/package/{channel}/'
    channel_url = f'https://anaconda.org/{channel}/'

    try:
        meta = http_request(f'{api_url}{pkgname}')
    except Exception:
        log.debug(f'Package lookup failed [{pkgname} : {channel_url}]')
        return None

    # Find the latest available
    # version for this platform
    thisplat = ('noarch', conda_platform_identifier())

    for finfo in meta['files'][::-1]:
        version  = finfo['version']
        platform = finfo['attrs']['subdir']
        buildno  = finfo['attrs']['build_number']
        build    = finfo['attrs']['build']

        if platform in thisplat:
            return Package(pkgname, version, buildno,
                           build, channel_url, platform)

    # No suitable version available
    return None


@ft.lru_cache
def download_channel_metadata(
        channel_url : str, **kwargs) -> Tuple[Dict, Dict]:
    """Downloads information about packages hosted at the given conda channel.

    Returns two dictionaries:

     - The first contains the contents of <channel_url>/channeldata.json, which
       contains information about all packages on the channel, and the
       platforms for which they are available.

     - The second contains the contents of
       <channel_url>/<platform>/repodata.json for all platforms on the
       channel. This dictionary has structure

           {platform : {pkgname : [ <pkginfo> ]}},

       where <pkginfo> contains the contents of an entry for a single package
       file entry from the "packages" section of a repodata.json file.

    Keyword arguments are passed through to the http_request function.
    """

    thisplat = ('noarch', conda_platform_identifier())

    # Load channel and platform metadata - the
    # first gives us a list of all packages that
    # are hosted on the channel and platforms
    # they are built for, and the second gives us
    # the available versions, and dependencies of
    # each package.
    chandata = http_request(f'{channel_url}/channeldata.json', **kwargs)
    chandata['channel_url'] = channel_url
    platdata = {}

    # only consider packages
    # relevant to this platform
    for platform in chandata['subdirs']:
        if platform in thisplat:
            purl               = f'{channel_url}/{platform}/repodata.json'
            platdata[platform] = http_request(purl)

    # Re-arrange the platform repodata to
    # {platform : {pkgname : [pkgfiles]}}
    # dicts, to make lookup by name easier.
    platdatadict = defaultdict(lambda : defaultdict(list))
    for platform, pdata in platdata.items():
        for pkg in pdata['packages'].values():
            platdatadict[platform][pkg['name']].append(pkg)
    platdata = platdatadict

    return chandata, platdata


def parse_channel_metadata(
        channeldata : List[Tuple[Dict, Dict]],
        pkgnames    : Sequence[str]
) -> Dict[str, Package]:
    """Extract metadata about the requested packages from the channel metadata.

    Parses the channel metadata, and creates a Package object for every
    requested package.

    Returns a dict of {name : Package} mappings.

    channeldata:  Sequence of channel data from one or more conda channels, as
                  returned by the download_channel_metadata function.
    pkgnames:     Sequence of package names to return metadata for
    """

    # Create Package objects for every available version of
    # the requested packages. Information about available
    # versions is not necessarily sorted, so we have to
    # parse and sort every entry to find the most recent.
    #
    # The packages dict has structure
    #
    # {pkgname : [Package, Package, ...]}
    #
    # where the package lists are sorted from oldest to
    # newest.
    #
    # The channel that a package is sourced from is not
    # considered in this order - we just want the newest
    # available version. In conda terms, this is equivalent
    # to setting the channel priority to "disabled".
    packages = defaultdict(list)
    for pkgname in pkgnames:
        for cdata, pdata in channeldata:
            if pkgname not in cdata['packages']:
                continue
            curl = cdata['channel_url']
            for platform in pdata.keys():
                for pkgfile in pdata[platform][pkgname]:
                    version   = pkgfile['version']
                    buildno   = pkgfile['build_number']
                    build     = pkgfile['build']
                    depends   = pkgfile['depends']
                    pkg       = Package(pkgname, version, buildno,
                                        build, curl, platform, depends)
                    bisect.insort(packages[pkgname], pkg)

    for pkgname in pkgnames:
        if pkgname not in packages:
            log.debug(f'Package {pkgname} is not available - ignoring.')

    # After sorting from oldest->newest we can just
    # return the newest version for each package
    return {pkgname : pkgs[-1] for pkgname, pkgs in packages.items()}


def is_internal(pkg : Union[Package, str], **kwargs) -> bool:
    """Determines whether the given package (a Package object or a name)
    is internally hosted/managed.

    Keyword arguments are assumed to be a username/password to be passed
    through to the http_request function, for accessing the internal
    FSL conda channel.
    """

    channels = [PUBLIC_FSL_CHANNEL,
                DEVELOPMENT_FSL_CHANNEL,
                INTERNAL_FSL_CHANNEL]

    if isinstance(pkg, str):
        chandata = [download_channel_metadata(PUBLIC_FSL_CHANNEL),
                    download_channel_metadata(DEVELOPMENT_FSL_CHANNEL),
                    download_channel_metadata(INTERNAL_FSL_CHANNEL, **kwargs)]
        pkgs = parse_channel_metadata(chandata, [pkg])
        return pkg in pkgs

    pkgchannel = urlparse.urlparse(pkg.channel).path.strip('/')
    channels   = [urlparse.urlparse(c).path.strip('/') for c in channels]
    return pkgchannel in channels or pkg.name in EXTERNALLY_HOSTED_PACKAGES


def get_fsl_environments(fsldir : str = None) -> Dict[str, str]:
    """Return a dict containing all conda environments in the given FSL
    installation (defaults to $FSLDIR). The returned dictionary has the form:

        {envname : envdir}

    A FSL installation comprises one main conda environment ($FSLDIR itself),
    and may contain additional child environments in $FSLDIR/envs/, containing
    optional components.  The envname for the main FSL environment is 'FSL',
    and for child environments is the name of the child environment directory
    (i.e. $FSLDIR/envs/<envname>/).
    """

    envs   = {'FSL' : fsldir}
    for childenvdir in glob.glob(op.join(fsldir, 'envs', '*')):
        envname       = op.basename(childenvdir)
        envs[envname] = childenvdir
    return envs


@ft.lru_cache
def query_installed_packages(envdir   : str  = None,
                             internal : bool = True) -> Dict[str, Package]:
    """Uses conda to find out the versions of all packages installed in
    envdir, and which are sourced from the FSL conda channels, or
    which are listed in EXTERNALLY_HOSTED_PACKAGES.

    Returns a dict of {pkgname : Package} mappings. The "dependencies"
    attributes of the package objects are not populated.

    :arg envdir:   Conda environment to query - defaults to the $FSLDIR
                   environment variable.

    :arg internal: If True (default), only internally hosted / managed
                   packages are returned.
    """

    if envdir is None:
        envdir = os.environ['FSLDIR']

    condabin = conda()
    cmd      = f'{condabin} list -p {envdir} --json'
    info     = json.loads(Process.check_output(cmd))
    pkgs     = {}

    # conda list returns a list of dicts,
    # one per package. We re-arrange this
    # into a dict of {pkgname : Package}
    # mappings.
    for pkg in info:
        pkg = Package(pkg['name'],
                      pkg['version'],
                      pkg['build_number'],
                      pkg['build_string'],
                      pkg['channel'],
                      pkg['platform'])

        if (not internal) or is_internal(pkg):
            pkgs[pkg.name] = pkg

    return pkgs


def get_environment_revisions(envdir : str = None) -> list:
    """Returns information about the revisions of the specified conda
    environment by scraping the $FSLDIR/conda-meta/history file.

    :arg envdir: Conda environment to query - defaults to the $FSLDIR
                 environment variable.
    :returns:    A list of dictionaries, one for each revision, containing
                 `'date'` and `'command'`.
    """
    if envdir is None:
        envdir = os.environ['FSLDIR']

    revs = []

    # Extract the revisions date and command
    # from conda-meta/history by searching for
    # line pairs of the form:
    #
    #     ==> <date> <==
    #     # cmd: <command>
    #
    history = op.join(envdir, 'conda-meta', 'history')
    with open(history, 'rt') as f:
        lines = f.read().strip().split('\n')
        lines = [l.strip() for l in lines]

    datepat = re.compile(r'==> (.+) <==')
    cmdpat  = re.compile(r'# cmd: (.+)')
    for i in range(len(lines) - 1):

        dateline = lines[i]
        cmdline  = lines[i + 1]

        datematch = re.fullmatch(datepat, dateline)
        cmdmatch  = re.fullmatch(cmdpat,  cmdline)

        if datematch and cmdmatch:
            date = datematch.group(1)
            cmd  = cmdmatch.group(1)

            # The first entry for miniconda/miniforge-based
            # installations will be from the initial
            # miniconda environment setup, originating from
            # conda constructor.
            if 'constructor' not in cmd:
                revs.append({'date' : date, 'command' : cmd})

    return revs


def parse_environment_file_name(filename):
    """Parses the given FSL environment file name, returning the version,
    platform, and environment name. The environment name will be None if the
    file name does not contain one.

    FSL environment files follow a consistent naming convention of the form:

        fsl-<version>[_<envname>]_<platform>.yml

    e.g.:

        fsl-6.0.8_macos-64.yml
        fsl-6.0.8_truenet_macos-64.yml

    All returned values will be None if the file name does not conform to
    this convention.

    The <envname> field is only present for optional add-on components that
    are installed as child environments in $FSLDIR/envs/<envname>/.
    """

    pat      = r'fsl-([^_]+)(?:_([^_]+))?_([^_]+)\.yml'
    filename = op.basename(filename)
    match    = re.fullmatch(pat, filename)

    if match is not None: return match.groups()
    else:                 return (None, None, None)


def read_environment_file(filename : str,
                          internal : bool = True,
                          **kwargs) -> Dict[str, Package]:
    """Reads a conda environment file. Returns a dict of {pkgname : version}
    mappings.

    :arg filename: Name of yml file to load
    :arg internal: If True (default), only internally hosted/managed packages
                   are returned.

    Keyword arguments are assumed to be a username/password to be passed
    through to the http_request function, for accessing the internal
    FSL conda channel.
    """
    with open(filename, 'rt') as f:
        env = yaml.load(f.read(), Loader=yaml.BaseLoader)

    pkgs = {}

    for pkg in env['dependencies']:
        bits = pkg.split(' ')
        if len(bits) == 1: name, ver = bits[0], None
        else:              name, ver = bits[:2]

        if (not internal) or is_internal(name, **kwargs):
            pkgs[name] = ver

    return pkgs


def update_fsl_channels(fsldir      : str,
                        outfile     : str,
                        development : bool = False,
                        internal    : bool = False):
    """Generates a .condarc file, suitable for installing into
    $FSLDIR/.condarc, so that the channel list contains the development and
    internal channels, as dictated by the input parameters.
    """

    channels = [PUBLIC_FSL_CHANNEL, 'conda-forge']

    if internal:    channels.insert(0, INTERNAL_FSL_CHANNEL)
    if development: channels.insert(0, DEVELOPMENT_FSL_CHANNEL)

    condarc = generate_condarc(fsldir, channels)
    with open(outfile, 'wt') as f:
        f.write(condarc)
