#!/usr/bin/env python
"""The update_fsl_release command can be used to either:

 - Update a FSL installation to the latest available FSL release.
 - Install an optional FSL component (as an extra/child conda environment)

To update a FSL installation, the command is called without any arguments,
i.e. "update_fsl_release". When updating a FSL installation, the following tasks
are performed:

1. Downloads the FSL release manifest file from the
   ``fsl.installer.fslinstaller.FSL_RELEASE_MANIFEST`` url.

2. Identifies the installed FSL version, and the latest available FSL version.

3. Determines whether an update is possible.

4. Runs "conda env update"

To install an additional FSL component, the command is called with the --extra
argument, e.g. "update_fsl_release --extra truenet". When installing an extra
environment, the following steps are performed:

1. Downloads the FSL release manifest file from the
   ``fsl.installer.fslinstaller.FSL_RELEASE_MANIFEST`` url.

2. Identifies the installed FSL version, and the extra environments that are
   available for that version.

3. Runs "conda env create"
"""


import os.path        as op
import                   os
import                   sys
import                   argparse
import functools      as ft

import fsl.base.conda as conda
import fsl.installer  as fi


@ft.cache
def installed_fsl_version():
    """Reads and returns the contents of ``$FSLDIR/etc/fslversion``. """
    fsldir     = os.environ['FSLDIR']
    fslversion = op.join(fsldir, 'etc', 'fslversion')
    if not op.exists(fslversion):
        return None
    with open(fslversion, 'rt') as f:
        fslversion = f.read()
    return fslversion.strip().split(':')[0]


def do_update(fsldir, oldver, ctx):
    """Called by update_fsl. Runs ``conda env update`` to update the FSL
    installation.

    :arg fsldir:   Location of FSL installation to update.
    :arg oldver:   Currently installed FSL version.
    :arg ctx:      fslinstaller.Context object, containing information
                   about the new FSL version.
    """

    build    = ctx.build
    newver   = build['version']
    condabin = conda.conda()
    envfile  = ctx.environment_file
    cmd      = f'{condabin} env update -f {envfile} -p {fsldir}'

    # Number of lines of expected output when
    # upgrading from the <installed> version
    # to the new version, used for progress
    # reporting. May or may not be available
    # in the manifest.
    output = build['output'].get(oldver, None)
    if output is not None:
        output = int(output)

    fi.printmsg(f'Updating {fsldir} from version {oldver} to {newver}...',
                fi.INFO)

    # The Context.run method makes sure the
    # shell environment is set up to perform
    # the update correctly.
    ctx.run(fi.Process.monitor_progress, cmd, total=output)
    fi.finalise_installation(ctx)


def update_fsl(ctx):
    """Updates the FSL installation to the latest available version. """
    # Retrieve the FSL release build entry,
    # from the manifest and figure out
    # whether an update is viable
    fsldir    = ctx.destdir
    build     = ctx.build
    installed = installed_fsl_version()
    available = build['version']

    # non-official FSL installation?
    if installed is None:
        fi.printmsg('Cannot identify version of FSL installation '
                    f'[{fsldir}]! Aborting update', fi.ERROR, fi.EMPHASIS)
        return 1

    # Already up to date?
    if fi.Version(installed) >= fi.Version(available):
        fi.printmsg(f'Your FSL installation ({fsldir}) is up to date\n'
                    f'  Installed version:        {installed}\n'
                    f'  Latest available version: {available}', fi.INFO)
        return 0

    fi.printmsg('A new version of FSL is available\n', fi.IMPORTANT,
                '  FSL installation directory: ',      fi.EMPHASIS,
                f'{fsldir}\n',
                '  Installed version:          ',      fi.EMPHASIS,
                f'{installed}\n',
                '  Latest available version:   ',      fi.EMPHASIS,
                f'{available}\n',                      fi.INFO)

    if not ctx.args.yes:
        response = fi.prompt('Do you want to update your '
                             'FSL installation? [y/n]:', fi.QUESTION)
        if response.lower() != 'y':
            fi.printmsg('Aborting update', fi.INFO)
            return 0

    do_update(fsldir, installed, ctx)

    fi.printmsg(f'\nFSL [{fsldir}] successfully updated '
                f'to version {available}', fi.IMPORTANT)


def install_extra(ctx):
    """Installs an optional FSL component as an extra child conda environment.
    """

    extras     = ctx.args.extra
    fslversion = installed_fsl_version()
    available  = ctx.build.get('extras', [])

    for extra in extras:
        if extra not in available:
            fi.printmsg(f'Requested extra component "{extra}" is not '
                        f'available in FSL {fslversion}!\n',
                        fi.ERROR, fi.EMPHASIS,
                        f'Available extras: {", ".join(available)}',
                        fi.INFO)
            return 1

    fi.printmsg(f'\nInstalling extra FSL components: {", ".join(extras)}\n',
                fi.INFO, fi.EMPHASIS)

    if not ctx.args.yes:
        response = fi.prompt('Proceed? [y/n]:', fi.QUESTION)
        if response.lower() != 'y':
            fi.printmsg('Aborting installation', fi.INFO)
            return 0

    for extra in extras:
        fi.install_extra(ctx, extra)


def main(argv=None):
    """Entry point for ``update_fsl_release``. Determines whether a new version
    of FSL is available and, if possible, updates $FSLDIR to that version.
    """

    if os.getuid() == 0:
        fi.printmsg('Running the update_fsl_release script as root user is '
                    'discouraged! You should run this script as a regular user '
                    '- you will be asked for your administrator password if '
                    'required.', fi.WARNING, fi.EMPHASIS)

    # This script will update the FSL
    # installation at $FSLDIR.  This may
    # not be the same as the environment
    # in which this script is installed,
    # which can be useful for testing/
    # debugging.
    try:
        fsldir = os.environ['FSLDIR']
    except KeyError:
        fi.printmsg('$FSLDIR is not set - aborting', fi.ERROR, fi.EMPHASIS)
        return 1

    # We create a fslinstaller.Context,
    # which handles downloading and
    # parsing the FSL release manifest.
    parser  = argparse.ArgumentParser()
    parser.add_argument('-y', '--yes', action='store_true',
                        help='Update FSL without prompting for confirmation')

    # Accept the --extra argument for
    # installing optional FSL components,
    # and some other arguments for
    # advanced usage and testing
    options = ['username',  'password', 'devrelease', 'devlatest',
               'manifest', 'workdir', 'channel', 'conda', 'extra',
               'extras_dir', 'cuda', 'skip_ssl_verify', 'logfile',
               'throttle_downloads', 'num_retries']

    args = fi.parse_args(argv, options, parser)

    # The user wants to install an optional
    # FSL component into their existing
    # FSL installation - pin the FSL version,
    # as extra environments are coupled to
    # specific FSL releases
    if len(args.extra) > 0:
        args.fslversion = installed_fsl_version()
        action          = 'install extras'

    # The user wants to update their FSL
    # installation to the newest version
    else:
        action = 'update FSL'

    ctx = fi.Context(args, fsldir, action=action)

    with fi.tempdir(args.workdir):

        logfile = fi.config_logging('update_fsl_release_',
                                    args.workdir, args.logfile)
        fi.printmsg(f'Log file: {logfile}\n', fi.INFO)

        fi.download_fsl_environment_files(ctx)

        if len(args.extra) > 0: return install_extra(ctx)
        else:                   return update_fsl(ctx)


if __name__ == '__main__':
    sys.exit(main())
