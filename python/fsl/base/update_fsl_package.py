#!/usr/bin/env python
"""Update one or more FSL packages using conda.

This script is only intended to be used within conda-based FSL installations
that have been created with the fslinstaller.py script. It is not intended to
be used within conda environments that have had FSL packages installed into
them - in this scenario, conda should be used directly.

The script performs the following steps:

 1. Parses command line arguments (primarily the list of packages to
    update).

 2. Queries the FSL conda channel(s) to find the latest available versions of
    the requested packages.

 4. Runs "conda install" to update the packages.

Currently this script cannot be used to install new packages - it is limited
to updating packages that are already installed. To install new packages,
conda should be used directly.
"""


import            argparse
import            logging
import os.path as op
import            os
import            sys

from typing import Dict, List, Optional, Sequence

import fsl.base.conda as conda
import fsl.installer  as fi


log = logging.getLogger(__name__)


def filter_packages(
        packages : Dict[str, conda.Package]) -> List[conda.Package]:
    """Identifies the versions of packages that should be installed.

    Removes packages that are not installed, or that are already up to date.

    Returns a list of Package objects representing the packages/versions to
    install.
    """

    filtered = []

    for pkgname, pkg in packages.items():

        # Find the Package object corresponding
        # to the installed version
        installed = conda.query_installed_packages().get(pkgname, None)

        if not installed:
            log.debug(f'Package {pkgname} is not installed - ignoring')
            continue

        if pkg <= installed:
            log.debug(f'{pkg.name} is already up to date (available: '
                      f'{pkg.version}, installed: {installed.version}) '
                      '- ignoring.')
        else:
            filtered.append(pkg)

    return filtered


def confirm_installation(
        packages : Sequence[conda.Package],
        yes      : bool) -> bool:
    """Asks the user for confirmation, before installing/updating the requested
    packages.
    """

    rows = []

    # Currently all requested packages should already
    # be installed, so a package should never be "n/a".
    # This might change in the future if this script
    # is enhanced to allow installation of new packages.
    for pkg in packages:
        installed    = conda.query_installed_packages().get(pkg.name, 'n/a')
        installedver = f'{installed.version} (build {installed.build})'
        pkgver       = f'{pkg.version} (build {pkg.build})'
        rows.append((pkg.name, installedver, pkgver))

    rows.insert(0, ('Package name', 'Installed', 'Available'))

    len0 = max(len(r[0]) for r in rows)
    len1 = max(len(r[1]) for r in rows)
    len2 = max(len(r[2]) for r in rows)

    rows.insert(1, ('-' * len0, '-' * len1, '-' * len2))

    template = f'{{:{len0}}}  {{:{len1}}}  {{:{len2}}}'

    print('\nThe following updates are available:\n')

    for row in rows:
        print(template.format(*row))

    if yes:
        return True

    response = input('\nProceed? [Y/n]: ')

    return response.strip().lower() in ('', 'y', 'yes')


def install_packages(fsldir       : str,
                     packages     : Sequence[conda.Package],
                     channels     : Sequence[str],
                     username     : str  = None,
                     password     : str  = None,
                     dry_run      : bool = False):
    """Calls conda to update the given collection of packages. """

    condabin = conda.conda()
    packages = [f'"{p.name}={p.version}=*{p.build}"' for p in packages]

    cmd  = f'{condabin} install -p {fsldir} -y '

    for channel in channels:
        cmd += f'-c {channel} '
    cmd += ' '.join(packages)

    print('\nInstalling packages...')
    if dry_run:
        print(f'Dry run - would execute {cmd}')
        return

    # Update the $FSLDIR/.condarc file if we are
    # using the internal/dev channels, as the
    # .condarc is written so that the channel
    # list is locked-down and can't be overridden
    # on the command-line.
    internal = conda.INTERNAL_FSL_CHANNEL    in channels
    devel    = conda.DEVELOPMENT_FSL_CHANNEL in channels

    env      = fi.clean_environ()
    appenv   = fi.install_environ(fsldir, username, password)
    admin    = fi.check_need_admin(fsldir)
    adminpwd = None

    if admin:
        adminpwd = fi.get_admin_password('update FSL packages')

    procargs = {'admin'        : admin,
                'password'     : adminpwd,
                'env'          : env,
                'append_env'   : appenv,
                'print_output' : True}

    # [Re-]generate the $FSLDIR/.condarc
    # file to make sure the channel list
    # contains devel/internal channels if
    # needed
    if op.exists(op.join(fsldir, '.condarc')):
        with fi.tempdir():
            conda.update_fsl_channels(fsldir, '.condarc', devel, internal)
            fi.Process.check_call(f'cp -f .condarc {fsldir}', **procargs)

    fi.Process.check_call(cmd, **procargs)


def parse_args(argv : Optional[Sequence[str]]) -> argparse.Namespace:
    """Parses command-line arguments, returning an argparse.Namespace object.
    """

    parser = argparse.ArgumentParser(
        'update_fsl_package', description='Update FSL packages')
    parser.add_argument('package', nargs='*',
                        help='Package[s] to update')
    parser.add_argument('-d', '--development', action='store_true',
                        help='Install development versions of '
                             'packages if available')
    parser.add_argument('-y', '--yes', action='store_true',
                        help='Install package[s] without prompting '
                             'for confirmation')
    parser.add_argument('-a', '--all', action='store_true',
                        help='Update all installed FSL packages')
    parser.add_argument('-e', '--external', action='store_true',
                        help='Consider externally hosted packages')
    parser.add_argument('-u', '--update', action='store_true',
                        help='Deprecated, has no effect')

    # Use conda instead of mamba
    # NOTE: This argument is no longer used
    parser.add_argument('--conda',    help=argparse.SUPPRESS)

    # consider packages from internal
    # channel (and use username/password
    # for auth to internal channel)
    parser.add_argument('--internal', help=argparse.SUPPRESS,
                        action='store_true')
    parser.add_argument('--username', help=argparse.SUPPRESS)
    parser.add_argument('--password', help=argparse.SUPPRESS)

    # print some more information about
    # which packages are to be updated
    parser.add_argument('--verbose',  help=argparse.SUPPRESS,
                        action='store_true')

    # print out the conda command
    # that would be executed
    parser.add_argument('--dry_run',  help=argparse.SUPPRESS,
                        action='store_true')

    args = parser.parse_args(argv)

    if args.username is None:
        args.username = os.environ.get('FSLCONDA_USERNAME', None)
    if args.password is None:
        args.password = os.environ.get('FSLCONDA_PASSWORD', None)

    if len(args.package) == 0 and not args.all:
        parser.error('Specify at least one package, or use --all '
                     'to update all installed FSL packages.')

    # externally hosted package has been requested
    if (len(args.package) > 0) and \
       any(p in conda.EXTERNALLY_HOSTED_PACKAGES for p in args.package):
        args.external = True

    return args


def main(argv : Sequence[str] = None):
    """Entry point. Parses command-line arguments, then installs/updates the
    specified packages.
    """

    try:
        fsldir = os.environ['FSLDIR']
    except Exception:
        print('$FSLDIR is not set - aborting')
        return 1

    if argv is None:
        argv = sys.argv[1:]

    args = parse_args(argv)

    if args.verbose: log.setLevel(logging.DEBUG)
    else:            log.setLevel(logging.INFO)

    # Build a list of all packages
    # to consider for the update
    print('Building FSL package list...')
    if args.all:
        pkgnames = list(conda.query_installed_packages().keys())
    else:
        pkgnames = args.package

    # build a dict of {pkgname : Package}
    # mappings for all candidate packages
    packages = {}

    # We start with externally hosted
    # packages (e.g. from conda-forge),
    # if requested
    if args.external:
        if args.all:
            extpkgs = list(conda.EXTERNALLY_HOSTED_PACKAGES.keys())
        else:
            extpkgs = [p for p in pkgnames
                       if p in conda.EXTERNALLY_HOSTED_PACKAGES]

        print('Downloading externally hosted package metadata...')
        for pkgname in extpkgs:
            pkg = conda.download_package_metadata(
                pkgname,
                conda.EXTERNALLY_HOSTED_PACKAGES[pkgname])
            if pkg is not None:
                packages[pkgname] = pkg

    # Download information about all available
    # packages on the FSL conda channels.
    print('Downloading FSL conda channel metadata...')
    channels    = [conda.PUBLIC_FSL_CHANNEL, 'conda-forge']
    channeldata = [conda.download_channel_metadata(conda.PUBLIC_FSL_CHANNEL)]
    if args.internal:
        channels   .insert(0, conda.INTERNAL_FSL_CHANNEL)
        channeldata.insert(0, conda.download_channel_metadata(
            conda.INTERNAL_FSL_CHANNEL,
            username=args.username,
            password=args.password))
    if args.development:
        channels   .insert(0, conda.DEVELOPMENT_FSL_CHANNEL)
        channeldata.insert(0, conda.download_channel_metadata(
            conda.DEVELOPMENT_FSL_CHANNEL))

    # Extract metadata for all requested
    # packages from the FSL channel metadata
    packages.update(conda.parse_channel_metadata(channeldata, pkgnames))

    # Remove packages which are already up to date
    packages = filter_packages(packages)

    if len(packages) == 0:
        print('\nNo packages need updating.')
    elif confirm_installation(packages, args.yes):
        install_packages(fsldir, packages, channels,
                         args.username, args.password,
                         args.dry_run)
    else:
        print('Aborting update')


if __name__ == '__main__':
    sys.exit(main())
