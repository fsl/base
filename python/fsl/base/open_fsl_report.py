#!/usr/bin/env python
#
# open_fsl_report.py - Open a web page in a web browser via a local web server.
#                      Used for opening local html files which refer to
#                      javascript files, to avoid cross-origin restrictions.
#

import                    argparse
import                    contextlib
import functools       as ft
import http.server     as http
import os.path         as op
import                    os
import multiprocessing as mp
import                    sys
import                    time
import                    webbrowser


@contextlib.contextmanager
def indir(dir):
    """Context manager which temporarily changes into dir."""
    prevdir = os.getcwd()
    os.chdir(dir)
    try:
        yield
    finally:
        os.chdir(prevdir)


class HTTPServer(mp.Process):
    """Simple HTTP server which serves files from a specified directory.

    Intended to be used via the server static method.
    """

    @contextlib.contextmanager
    @staticmethod
    def server(rootdir=None):
        """Start a HTTPServer on a separate thread to serve files from
        rootdir (defaults to the current working directory), then shut it down
        afterwards.
        """
        if rootdir is None:
            rootdir = os.getcwd()
        srv = HTTPServer(rootdir)
        srv.start()
        # wait until server has started
        srv.startup.wait()
        srv.url = f'http://localhost:{srv.port}'
        try:
            yield srv
        finally:
            srv.stop()

    def __init__(self, rootdir):
        mp.Process.__init__(self)
        self.daemon         = True
        self.rootdir        = rootdir
        self.portval        = mp.Value('i', -1)
        self.startup        = mp.Event()
        self.shutdown       = mp.Event()
        self.requestcounter = mp.Value('i', 0)

    def stop(self):
        self.shutdown.set()

    def is_running(self):
        return not self.shutdown.is_set()

    @property
    def port(self):
        return self.portval.value

    @property
    def numrequests(self):
        return self.requestcounter.value

    def run(self):
        try:
            # Use a custom HTTPRequestHandler
            # class to count successful requests.
            handler = HTTPRequestHandler.factory(self.requestcounter)
            server  = http.HTTPServer(('', 0), handler)

            # store port number, notify startup
            self.portval.value = server.server_address[1]
            self.startup.set()

            # Configure the handle_request method
            # to wait for up to half a second for
            # a request before returning.
            server.timeout = 0.5

            # Serve until the stop() method is called.
            with indir(self.rootdir):
                while not self.shutdown.is_set():
                    server.handle_request()
                server.shutdown()
        # set the shutdown event on error
        finally:
            self.stop()


class HTTPRequestHandler(http.SimpleHTTPRequestHandler):
    """Custom HTTPRequestHandler which updates a shared multiprocessing.Value
    instance whenever a successful HTTP 200 request is processed.
    """

    @staticmethod
    def factory(requestcounter):
        return ft.partial(HTTPRequestHandler, requestcounter)

    def __init__(self, requestcounter, *args, **kwargs):
        self.requestcounter = requestcounter
        super().__init__(*args, **kwargs)

    def log_message(self, *args, **kwargs):
        """Suppress logging output. """

    def log_request(self, code='-', size='-'):
        """Increment the request counter on HTTP 200. """
        super().log_request(code, size)
        if code == 200:
            self.requestcounter.value += 1


def parseArgs(argv):
    parser = argparse.ArgumentParser(
        'open_fsl_report', description='Open a FSL HTML report file')
    parser.add_argument('htmlfile', help='File to open', type=op.abspath)
    parser.add_argument('-s', '--seconds', type=int, default=10,
                        help='Number of seconds to run the web server '
                        'for (default: 10).')
    parser.add_argument('-r', '--requests', type=int, default=999,
                        help='Number of HTTP requests to serve before '
                        'shutting down the web server (default: 999).')

    if argv is None:
        argv = sys.argv[1:]

    return parser.parse_args(argv)


def main(argv=None):
    """Entry point. Starts a web server, then opens the requested HTML file in
    the default web browser.
    """
    args     = parseArgs(argv)
    dirname  = op.dirname( args.htmlfile)
    filename = op.basename(args.htmlfile)
    elapsed  = 0

    with HTTPServer.server(dirname) as srv:
        url = f'{srv.url}/{filename}'
        webbrowser.open_new_tab(url)

        while all((srv.numrequests < args.requests,
                   elapsed         < args.seconds,
                   srv.is_running())):
            time.sleep(0.5)
            elapsed += 0.5

        srv.shutdown.set()


if __name__ == '__main__':
    main()
