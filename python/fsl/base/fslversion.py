#!/usr/bin/env python
#
# This script implements the "fslversion" command, which prints information
# about the installed FSL version.
#


import            argparse
import            glob
import            os
import os.path as op
import            re
import            sys

from typing import Dict, List, Tuple, Optional, Sequence

import fsl.base.conda as conda
import fsl.installer  as finst


def read_fslversion(fsldir : str) -> Optional[str]:
    """Returns the contents of the $FSLDIR/etc/fslversion file, or None if
    the file does not exist.
    """
    fslverfile = op.join(fsldir, 'etc', 'fslversion')
    if op.exists(fslverfile):
        with open(fslverfile, 'rt') as f:
            return f.read().strip()
    return None


def get_fsldir_revision_history(
        fsldir : str
) -> Tuple[List[Tuple[str, str, str]], bool]:
    """Retrieves the revision history of the given FSL installation.

    Also determines whether this looks like a "standard" FSL installation
    (normal installation, and all updates through the fsl_update_release
    command).

    Returns a tuple containing:

      - a list of (date, envname, command) tuples, one for each command that
        has been applied to the installation, sorted by date. The
        envname for the main FSL environment is 'FSL', and for child
        environments is the name of the child environment directory
        (i.e. $FSLDIR/envs/<envname>/).

      - a boolean indicating whether this appears to be a standard FSL
        installation (True), or whether the user has modified it in some
        non-standard way, including use of the update_fsl_package command
        (False).
    """

    # gather all environments we wish to interrogate
    fsldir  = op.realpath(op.abspath(fsldir)).rstrip(op.sep)
    envs    = conda.get_fsl_environments(fsldir)
    dirty   = False
    history = []

    for envname, envdir in envs.items():

        # Get conda env revision history
        revs = conda.get_environment_revisions(envdir)

        # Define regexes to match conda commands in the revision history.
        # Conda revision history for a standard FSL installation should
        # contain revisions of the form:
        #
        #   1. Initial installation of <releaseA> with fslinstaller.py:
        #      conda env update -p envdir -f <fsl-releaseA>.yml
        #
        #   N. Update to <releaseN> with update_fsl_release (one of):
        #      conda env update -p envdir -f <fsl-releaseN>.yml
        #      conda env update -f <fsl-releaseN>.yml -p envdir
        #
        # Revision history for optional child environments is the same,
        # except the initial installation will have been performed with
        # "conda env create" rather than "conda env update"
        #
        # We capture two groups:
        #   - the command - "update" or "create"
        #   - the environment file name
        cmdpat  = r'(?:/.*/)(?:conda|mamba|micromamba)(?: env|-env)? ' \
                  r'(update|create)'
        dirpat  = rf'{envdir}'
        filepat = r'(.*\.yml)'

        patterns = [rf'{cmdpat} -p {dirpat} -f {filepat}(?: |$)',
                    rf'{cmdpat} -f {filepat} -p {dirpat}(?: |$)']

        # Extract the date and conda command for each
        # revision, sanitising the commands a little.
        for i, rev in enumerate(revs):

            date  = rev['date']
            cmd   = rev['command']
            match = None

            for pat in patterns:
                match = re.match(pat, cmd)
                if match is not None:
                    break

            # If the regex matches, we parse the FSL environment file
            # name (as they follow a consistent naming convention) to
            # extract the FSL version number.  If this comes out as
            # None, it means the file was not a FSL environment file.
            fslver = None
            if match is not None:
                fslver = conda.parse_environment_file_name(match.group(2))[0]

            if fslver is not None:
                # first revision - initial installation
                if i == 0: cmd = f'Install {envname} {fslver}'
                else:      cmd = f'Update {envname} {fslver}'

            # Some other non-standard command
            # (including update_fsl_package)
            else:
                dirty = True
                cmd   = cmd.replace(fsldir, '$FSLDIR')
                words = list(cmd.split())

                # remove any $TMPDIR prefixes
                for i, word in enumerate(words):
                    if 'tmp' in word:
                        word = op.basename(word)
                    words[i] = word

                cmd = ' '.join(words)

            history.append((date, envname, cmd))

    # sort by date (oldest first)
    return sorted(history), not dirty


def print_fslversion(fsldir : str, verbose : bool):
    """Called by main. Prints the FSL version.

    If verbose is True, also prints information about each update to this FSL
    installation.
    """
    fslver         = read_fslversion(fsldir)
    revs, standard = get_fsldir_revision_history(fsldir)

    # Non-standard installation
    if fslver is None:
        fslver = 'Unknown [$FSLDIR/etc/fslversion does not exist]'

    # Non-standard transactions in $FSLDIR
    # environment revisions
    elif (not standard):
        fslver = f'{fslver} (modified)'

    print(f'FSLDIR:  {fsldir}')
    print(f'Version: {fslver}')

    if verbose and len(revs) > 0:
        maxnamelen = max(len(envname) for _, envname, _ in revs)
        print('Revisions:')
        for date, envname, cmd in revs:
            space = ' ' * (maxnamelen -  len(envname))
            print(f'  {date} [{envname}]:{space} {cmd}')


def print_installed_packages(fsldir : str, extra : bool):
    """Called by main if --packages is provided. Prints a list of all FSL
    packages that are installed.

    If the extra argument is False, only information about the main FSL
    environment is printed.
    """
    if extra: envs = conda.get_fsl_environments(fsldir)
    else:     envs = {'FSL' : fsldir}

    packages = {}
    for envname, envdir in envs.items():
        packages[envname] = conda.query_installed_packages(envdir)


    for envname, envpackages in packages.items():
        print(f'{envname} Packages:')
        names = []
        vers  = []
        for pkg in envpackages.values():
            names.append(pkg.name)
            vers .append(pkg.version)

        nlen = max(len(n) for n in names)
        vlen = max(len(v) for v in vers)
        fmt  = f'  {{:{nlen}s}} {{:{vlen}s}}'

        for name, ver in zip(names, vers):
            print(fmt.format(name, ver))


def create_conda_environment_file(fsldir : str, envfile : str, extra : bool):
    """Called by main if --envfile is provided. Creates a conda environment.yml
    file which fully specifies the FSL installation, using conda env export.

    If the extra argument is True, the envfile is interpreted as a prefix, and
    environment files for all FSL environments are generated.
    """
    if extra:
        fileprefix = envfile
        envs       = conda.get_fsl_environments(fsldir)
        envfiles   = {}

        for envname, envdir in envs.items():
            if envname == 'FSL': envfile = f'{fileprefix}.yml'
            else:                envfile = f'{fileprefix}_{envname}.yml'
            envfiles[envdir] = envfile
    else:
        envfiles = {fsldir : envfile}

    for envdir, envfile in envfiles.items():
        cmd = f'{conda.conda()} env export -p {envdir}'
        env = finst.Process.check_output(cmd)
        with open(envfile, 'wt') as f:
            f.write(env)


def parse_args(args : Sequence[str]) -> argparse.Namespace:
    """Parses command-line arguments. """

    helps = {
        'verbose'  : 'Print additional information.',
        'packages' : 'List all installed FSL packages.',
        'envfile'  : 'Create a conda environment file which fully describes '
                     'this FSL installation.',
        'extra'    : 'Print information about installed optional FSL '
                     'componenets. When used in conjunction with envfile, '
                     'the provided file name is assumed to be a prefix.',
    }

    parser = argparse.ArgumentParser(
        'fslversion', description='Report FSL version')
    parser.add_argument('-v', '--verbose',  help=helps['verbose'],
                        action='store_true')
    parser.add_argument('-p', '--packages', help=helps['packages'],
                        action='store_true')
    parser.add_argument('-f', '--envfile',  help=helps['envfile'],
                        metavar='FILE')
    parser.add_argument('-e', '--extra',    help=helps['extra'],
                        action='store_true')

    return parser.parse_args(args)


def main(argv : Sequence[str] = None):
    """Main routine for the fslversion command. """

    try:
        fsldir = os.environ['FSLDIR']
    except Exception:
        print('$FSLDIR is not set - aborting')
        return 1

    if argv is None:
        argv = sys.argv[1:]

    args = parse_args(argv)

    print_fslversion(fsldir, args.verbose)
    if args.packages: print_installed_packages(fsldir, args.extra)
    if args.envfile:  create_conda_environment_file(fsldir,
                                                    args.envfile,
                                                    args.extra)

    return 0


if __name__ == '__main__':
    sys.exit(main())
