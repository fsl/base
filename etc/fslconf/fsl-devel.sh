# FSL configuration file for developers
#
# This file must be sourced in order to compile
# Makefile-based FSL projects.
#
# $FSLDIR must be set before this script is called.
#
# Standard environment configuration for developing FSL is:
#   export FSLDIR=/usr/local/fsl
#   source $FSLDIR/etc/fslconf/fsl-devel.sh
#

# FSLCONFDIR contains the FSL Makefile machinery
# for compiling and installing FSL Makefile-based
# projects.
if [ ! -d "${FSLCONFDIR}" ]; then
  export FSLCONFDIR="${FSLDIR}/config"
fi

# FSLDEVDIR is an optional installation destination
# for development, so that locally built projects
# can be installed separately from FSLDIR.
#
# FSLDEVDIR doesn't necessarily have to exist, as
# it will be created the first time "make install"
# is run in a project directory.
if [ -z "${FSLDEVDIR}" ]; then
  export FSLDEVDIR="${FSLDIR}"
fi

source "${FSLDIR}/etc/fslconf/fsl.sh"
