# FSL configuration file
#  - to be sourced by the user, typically in .bashrc or equivalent
#
# $FSLDIR must be set before this script is called.
#
# Standard environment configuration for using FSL with csh is:
#   setenv FSLDIR /usr/local/fsl
#   source $FSLDIR/etc/fslconf/fsl.csh
#
# See fsl-devel.sh if you are compiling FSL projects.
#
# Written by Mark Jenkinson
#  FMRIB Analysis Group, University of Oxford

#### Set up standard FSL user environment variables ####

# If this is an official (fslinstaller-installed) FSL
# installation, wrappers to FSL executables will have
# been created in $FSLDIR/share/fsl/bin/ We add this to
# the $PATH, rather than adding $FSLDIR/bin/ to the
# path, to avoid clobbering the user's environment with
# other executalbes that have been installed into
# $FSLDIR/bin/ (e.g.  python).
#
# The fslinstaller creates a file called
# $FSLDIR/etc/fslversion when it installs official FSL
# installations.
#
# If this is a manually managed installation, it is the
# user's responsibility to add $FSLDIR/bin to the $PATH
if ( -f "$FSLDIR/etc/fslversion" ) then
  set PATH = ( $FSLDIR/share/fsl/bin:$PATH )
endif

# The following variable selects the default output image type
# Legal values are:
#   NIFTI
#   NIFTI2
#   NIFTI_PAIR
#   NIFTI2_PAIR
#   NIFTI_GZ
#   NIFTI2_GZ
#   NIFTI_PAIR_GZ
#   NIFTI2_PAIR_GZ
#
# This would typically be overwritten in the user's shell profile if the user
# wished to write files with a different format
setenv FSLOUTPUTTYPE NIFTI_GZ

# Comment out the definition of FSLMULTIFILEQUIT to enable
# FSL programs to soldier on after detecting multiple image
# files with the same basename ( e.g. epi.hdr and epi.nii )
setenv FSLMULTIFILEQUIT TRUE


# The following variables specify paths for programs and can be changed
# or replaced by different programs

setenv FSLTCLSH $FSLDIR/bin/fsltclsh
setenv FSLWISH $FSLDIR/bin/fslwish

# The following variables are used for running code in parallel across
#  several machines ( i.e. for FDT )

# setenv FSLLOCKDIR
# setenv FSLMACHINELIST
# setenv FSLREMOTECALL
# setenv FSLPARALLEL

# The following variable controls the loading of extensions by newimage.
# If 0 ( or unset ) newimage will _not_ load extensions, a value of 1
# will allow extensions to be loaded.
setenv FSL_LOAD_NIFTI_EXTENSIONS 0

# The following variable controls if "global" fsl startup functions should
# be run. If 0 ( or unset ) the functions will run, a value of 1 will
# bypass these functions. Currently this only affects Openblas threading.
setenv FSL_SKIP_GLOBAL 0

###################################################
### Add other global environment variables here ###
###      or change the definitions above        ###
###################################################


# USER VARIABLES HERE


###################################################
####    DO NOT ADD ANYTHING BELOW THIS LINE    ####
###################################################

if ( -f /usr/local/etc/fslconf/fsl.csh ) then
  source /usr/local/etc/fslconf/fsl.csh
endif


if ( -f /etc/fslconf/fsl.csh ) then
  source /etc/fslconf/fsl.csh
endif


if ( -f "${HOME}/.fslconf/fsl.csh" ) then
  source "${HOME}/.fslconf/fsl.csh"
endif
