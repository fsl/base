# This file may be installed into $FSLDIR/lib/pythonX.Y/site-packages/
# to add support for accessing Python modules that have been installed
# into the $FSLDEVDIR directory.
#
# It adds $FSLDEVDIR/lib/pythonX.Y/site-packages to the Python module
# search path.
#
# This file is not installed when running "make install" - it is
# installed by the fslinstaller.py script, or can be installed by
# system administrators as-needed.
#
# Refer to https://docs.python.org/3/library/site.html for more details.


import os
import sys
import glob

try:
    fsldev = os.environ['FSLDEVDIR']
    pydirs = glob.glob(os.path.join(fsldev, 'lib', 'python*'))

    for pydir in pydirs:
        sitedir = os.path.join(pydir, 'site-packages')
        if os.path.isdir(sitedir):
            sys.path.append(sitedir)

except KeyError:
    pass
