# Note that this Makefile does not use
# the FSL Makefile system, as that
# system is provided by this project.

MKDIR   ?= /bin/mkdir -m 0755
INSTALL ?= install -p
PREFIX  ?= ${FSLDEVDIR}

install:
	${FSLDIR}/bin/python -m pip install ./python          \
	      --prefix ${PREFIX} --no-deps --ignore-installed \
	      --no-cache-dir -vvv
	${MKDIR} -p ${PREFIX}/bin
	${MKDIR} -p ${PREFIX}/config
	${MKDIR} -p ${PREFIX}/doc/images
	${MKDIR} -p ${PREFIX}/etc/fslconf
	${MKDIR} -p ${PREFIX}/etc/flirtsch
	${MKDIR} -p ${PREFIX}/etc/js
	${MKDIR} -p ${PREFIX}/etc/luts
	${MKDIR} -p ${PREFIX}/etc/matlab
	${MKDIR} -p ${PREFIX}/share/fsl/copyrights
	${MKDIR} -p ${PREFIX}/share/fsl/sbin
	${MKDIR} -p ${PREFIX}/share/fsl/bin

	${INSTALL} -m 0755 LICENCE.FSL                 ${PREFIX}/
	${INSTALL} -m 0755 bin/*                       ${PREFIX}/bin/
	${INSTALL} -m 0755 bin/*                       ${PREFIX}/share/fsl/bin/
	${INSTALL} -m 0644 config/*                    ${PREFIX}/config/
	${INSTALL} -m 0755 config/supportedGencodes.sh ${PREFIX}/config/
	${INSTALL} -m 0644 doc/fsl.css                 ${PREFIX}/doc/
	${INSTALL} -m 0644 doc/images/*                ${PREFIX}/doc/images/
	${INSTALL} -m 0644 etc/fslconf/*               ${PREFIX}/etc/fslconf/
	${INSTALL} -m 0644 etc/flirtsch/*              ${PREFIX}/etc/flirtsch/
	${INSTALL} -m 0644 etc/fslconf/*               ${PREFIX}/etc/fslconf/
	${INSTALL} -m 0644 etc/js/*                    ${PREFIX}/etc/js/
	${INSTALL} -m 0644 etc/luts/*                  ${PREFIX}/etc/luts/
	${INSTALL} -m 0644 etc/matlab/*                ${PREFIX}/etc/matlab/
	${INSTALL} -m 0644 share/fsl/copyrights/*      ${PREFIX}/share/fsl/copyrights/
	${INSTALL} -m 0755 share/fsl/sbin/*            ${PREFIX}/share/fsl/sbin/
