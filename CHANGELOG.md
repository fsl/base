# FSL base project changelog


# 2502.2 (Friday 21st February 2025)

* Changed CUDA builds to statically link the CUDA toolkit by default. Dynamic linking can be selected by setting `CUDA_DYMANIC=1`.


# 2502.1 (Thursday 13th February 2025)

* The `fslipython`/`fslnotebook` commands will print an error if `ipython`/`jupyter-notebook` are not installed.

# 2502.0 (Monday 3rd February 2025)

* Update the C++ standard used for CUDA code to C++17.
* Fix a bug in the build rule for `.c` files.
* Add default compute capabilities for more CUDA versions.

# 2501.1 (Wednesday 22nd January 2025)

* Fixed an issue in the build rules regarding `-rpath` flags on macOS.


# 2501.0 (Monday 13th January 2025)

* Removed the `createFSLWrapper` and `removeFSLWrapper` scripts - these are now installed by the `fslinstaller.py` script.
* Adjustments for compatibility with `micromamba`.


# 2412.1 (Tuesday 24th December 2024)

* Further adjustment to recursive invocations of `make`, ensuring that compiler flags defined in the environment are passed through.


# 2412.0 (Friday 13th December 2024)

* Fixed a bug in the build rules affecting recursive invocations of `make`.


# 2411.0 (Tuesday 26th November 2024)

* Fixed a bug in the build rules for C compilation.


# 2410.0 (Thursday 31st October 2024)

* Adjust the `Makefile` rules so that `${FSLDEVDIR}/include` takes precedence over
  `${FSLDIR}/include`.


# 2408.0 (Friday 23rd August 2024)

* Re-wrote the automatic dependency generation logic in the FSL `Makefile` build system
  to allow for more flexibility.


# 2407.0 (Wednesday 24th July 2024)

* Fixed an issue in `update_fsl_release` related to installing extra environments.

# 2406.1 (Friday 21st June 2024)

* The `createFSLWrapper` script has been updated so that existing wrapper scripts are not
  overwritten. This is important in situations where a child conda environment is installed
  within `$FSLDIR` with packages that are also installed in the main FSL environment.


# 2406.0 (Thursday 13th June 2024)

* The `update_fsl_release` command can now be used to install additional FSL components ("extras") into an existing FSL installation, by using the `--extra` flag.
* The `fslversion` command will now print information about installed additional
  FSL components.


## 2405.0 (Friday 31st May 2024)

* Modified the `createFSLWrapper` and `removeFSLWrapper` scripts to allow creation of
  wrappers for executables installed into child environments within `$FSLDIR`.


## 2404.0 (Friday 5th April 2024)

* Fixed an issue with `update_fsl_package` attempting to install packages for the wrong platform.


## 2403.0 (Thursday 28th March 2024)

* Removed the obsolete `$FSLGECUDAQ` environment variable from the `fsl.[c]sh`
shell initialisation scripts.


## 2401.0 (Saturday 13th January 2024)

* A change to `update_fsl_package` to ensure that new builds of packages will
  be installed.


## 2310.0 (Tuesday 31st October 2023)


* On Linux, both `<Tool>` and `<Tool>_gui` links are created for TCL GUIs. This is so that instructions in e.g. the FSL Course practicals, can use the same commands on all platforms.
* Re-wrote the `createFSLWrapper` and `removeFSLWrapper` scripts in Python to allow more flexible functionality.


## 2309.2 (Friday 22nd September 2023)


* Add a `--yes` option to the `update_fsl_release` command, allowing the
  confirmation prompt to be skipped.
* Added some tests for the `open_fsl_report` command.


## 2309.1 (Tuesday 19th September 2023)


* New `open_fsl_report` command, for opening local HTML files which use
  JavaScript.


## 2309.0 (Monday 11th September 2023)


* The `USR*` `Makefile` variables can now be specified/overridden via
  environment variables.


## 2306.1 (Friday 30th June 2023)


* New `--conda` option for both `update_fsl_package` and `update_fsl_release`,
  which instructs them to use `conda` instead of `mamba`.


## 2306.0 (Wednesday 15th June 2023)


* New `fslnotebook` alias, which starts a Jupyter Notebook session with
  the FSL Python environment.


## 2303.4 (Thursday 23rd March 2023)


* New `custominstall` Make rule for any additional installation logic that a
  project may require. Intended to be used in place of (for example) the
  `fsl/flirt` `schedule` Make rule.


## 2303.3 (Monday 13th March 2023)


* Adjust the `update_fsl_packages` so that it calls `conda` *without* the
  `--freeze-installed` option, as otherwise `conda` frequently fails to
  resolve updates.


## 2303.2 (Wednesday 1st March 2023)


* Additional corrections to the `$FSLDIR/etc/fslconf/fsl.csh` script.


## 2303.1 (Thursday 2nd March 2023)


* Internal updates to the `update_fsl_package` script.
* Simplify detection of the python interpreter in the `createFSLWrapper`
  script.


## 2303.0 (Wednesday 1st March 2023)


* Corrections to the `$FSLDIR/etc/fslconf/fsl.csh` script.


## 2302.2 (Monday 27th February 2023)


* Small adjustments to the `createFSLWrapper` script.


## 2302.1 (Wednesday 15th February 2023)


* Updated `$FSLDIR/etc/fslconf/fsl.csh` to bring it in sync with
  `$FSLDIR/etc/fslconf/fsl.sh`


## 2302.0 (Thursday 9th February 2023)


* New `fslversion` command, which prints information about a FSL installation.
* Changed `$FSLDIR/bin/Runtcl` so that it does not check for a `$DISPLAY`
  environment variable (as this variable may not be present on macOS).


## 2301.1 (Tuesday 24th January 2023)


* The `$CXXFLAGS` environment variable is now propagated through to `nvcc`
  calls.
* The `$FSLDEVDIR` destination will be created if it doesn't already
  exist, when `make install` is run inside a FSL project directory.
* The `update_fsl_package` command will add the FSL conda development channel
  to `$FSLDIR/.condarc` if the `--development` option is used.


## 2301.0 (Monday 23rd January 2023)


* The `find_cuda_exe` command will return the oldest available executable if
  `nvidia-smi` reports a CUDA version older than any available executable.


## 2212.0 (Friday 2nd December 2022)


* The `update_fsl_package` and `update_fsl_release` commands will prefer
  `mamba` over `conda`, if the former is present.


## 2211.1 (Monday 28th November 2022)


* Fixed a bug in the `update_fsl_package` code where FSL platform identifiers
  were being used instead of conda platform identifiers.


## 2211.0 (Wednesday 16th November 2022)


* Adjust the `update_fsl_package` script so it does not query the `defaults`
  anaconda channel.


## 2209.3 (Tuesday 25th October 2022)


* Reverted to original FSL6.0 copyright date


## 2209.2 (Monday 24th October 2022)


* Add a "global" licence file to environment root


## 2209.1 (Friday 30th September 2022)


* Add a compiler flag to allow use of modern C++ features on macOS.


## 2209.0 (Tuesday 27th September 2022)


* New `update_fsl_release` script.
* Python scripts are now installed as a python library.


## 2205.9 (Tuesday 21st June 2022)


* Added exports for `FSL_LOAD_NIFTI_EXTENSIONS` and `FSL_SKIP_GLOBAL`


## 2205.8 (Wednesday 8th June 2022)


* Added support for `.cuh` files to the `make depend` rule.


## 2205.7 (Monday 30th May 2022)


* Fixed an issue with the `find_cuda_exe` script - it was crashing if
  `$FSLDEVDIR` was unset.
* Adjusted the `update_fsl_package` script so that it will install the latest
  available versions of all packages, regardless of which channel they are
  sourced from.


## 2205.6 (Tuesday 24th May 2022)


* Updated the `update_fsl_package` script to work the the separate FSL conda
  development channel at
  https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development/.


## 2205.5 (Monday 23rd May 2022)


* New `find_cuda_exe` script, for identifying a suitable CUDA executable to run
  given the version of CUDA that is installed.


## 2205.4 (Thursday 19th May 2022)


* Bug fix in the way that the `createFSLWrapper` and `removeFSLWrapper` scripts
  compare `$FSLDIR` to `$PREFIX` (the conda environment directory).


## 2205.3 (Thursday 19th May 2022)


* The `tclinstall` Make target (used to install `RUNTCLS` and `TCLFILES`) no
  longer calls the `auto_mkindex` command to generate/update
  `$FSLDIR/tcl/tclIndex`. Instead, a pre-generated `tclIndex` file is
  installed as part of the `misc_tcl` project.


## 2205.2 (Monday 16th May 2022)


* Fixed an issue with RPATHS not being set on CUDA binaries.


## 2205.1 (Monday 16th May 2022)


* Adjust NVCC linking flags to work on macOS.


## 2205.0 (Friday 13th May 2022)

* Adjust the CUDA version detection logic so that it works on both Linux and
  macOS.
* Some corrections to the `supportedGencodes.sh` script for CUDA 9.0 / 9.1.


## 2203.0 (Tuesday 2nd March 2022)

* On Linux, use `-llapack -lblas` to link, rather than `-lopenblas`, as this
  will allow the BLAS implementation to be changed without requiring
  recompilation (see
  https://conda-forge.org/docs/maintainer/knowledge_base.html#blas).


## 2202.4 (Monday 21st February 2022)

* Remove default flags from `supportedGencodes.sh` - they are not supported
  by newer CUDA versions. Instead, the script exits with an error if the CUDA
  version is unrecognised.


## 2202.3 (Friday 18th February 2022)

* Bug fix in `update_fsl_package`.


## 2202.2 (Friday 18th February 2022)

* Adjust `update_fsl_package` to take package build numbers into account.


## 2202.1 (Wednesday 16th February 2022)

* Bug fix in `config/supportedGencodes.sh`.


## 2202.0 (Wednesday 16th February 2022)

* Added entries for CUDA 11.5 and 11.6 to the `config/supportedGencodes.sh`
  script.


## 2201.3 (Tuesday 18th January 2022)

* Fixed a bug in the `tclinstall` target, in `$FSLDIR/config/rules.mk`.


## 2201.2 (Tuesday 4th January 2022)

* Fixed a bug in the `update_fsl_package` command.


## 2201.1 (Tuesday 4th January 2022)

* The `update_fsl_package` ensures that wrapper scripts in
  `$FSLDIR/share/fsl/bin/` are refreshed when updating FSL packages.


## 2112.6 (Thursday 31st December 2021)

* The `createFSLWrapper` script nowe has the ability to generate a wrapper
  script with a name different to the called executable.


## 2112.5 (Thursday 30th December 2021)

* New `update_fsl_package` script, for installing new versions of FSL packages.


## 2112.4 (Tuesday 14th December 2021)

* Fixed more issues with the `depend.mk` rule.
* Added a `fslipython` wrapper script for `$FSLDIR/bin/ipython`.


## 2112.3 (Monday 13th December 2021)

* Fixed an issue with variable quoting in the `depend.mk` rule.


## 2112.2 (Monday 13th December 2021)

* Fixed installation permissions on the `$FSLDIR/config/depend.sh` script.


## 2112.1 (Monday 13th December 2021)

* Adjust the `depend.mk` rule to support automatic dependency generation for
  `.cu` CUDA source files.


## 2112.0 (Saturday 11th December 2021)

* Add `-pthread` to the standard C++ compiler flags for `g++`.
* Adjust the `depend.mk` rule to allow saving object files to a separate
  build directory.


## 2111.9 (Wednesday 8th December 2021)

* If the `${LDFLAGS}` environment variable is set in the environment, any
  `-L` options are dropped from it, so that the `$FSLDEVDIR` can explicitly
  be set to take precedence over `$FSLDIR`.
* `${FSLDEVDIR}` doesn't have to exist - if it is set before
  `${FSLDIR}/etc/fslconf/fsl-devel.sh` is run, it will not be overridden.


## 2111.8 (Friday 26th November 2021)

* Work around an issue with compiling against older versions of `libxml++`,
  when using `-std=c++17`.
* The `FSLLOCKDIR`, `FSLMACHINELIST`, `FSLREMOTECALL` and `FSLPARALLEL`
  variables are not defined by default.


## 2111.7 (Friday 19th November 2021)

* Fixed an issue in the construction of `${NVCCFLAGS}` and `${CUDACXXFLAGS}`.


## 2111.6 (Friday 19th November 2021)

* New convention for compiling C++ files which are part of a CUDA library or
  executable, using `${CUDACXXFLAGS}`.


## 2111.5 (Friday 19th November 2021)

* Set `-std=c++14` for `nvcc`-compiled code, as CUDA 11.0 is the first version
  to support `-std=c++17`, and we are targeting older CUDA versions.


## 2111.4 (Wednesday 17th November 2021)

* The default C++ language standard is now `-std=c++17`.


## 2111.3 (Wednesday 17th November 2021)

* The `$FSLDEVDIR/lib` directory is now added to the `rpath` entry for shared
  libraries and executables, if `$FSLDEVDIR` is different from `$FSLDIR`.


## 2111.2 (Monday 15th November 2021)

* Added the `FSL_GE_606` `Makefile` flag for use by projects which need to
  retain support for compiling against older versions of FSL.


## 2111.1 (Friday 12th November 2021)

 - Tweak to how the `-install_name` option is set.


## 2111.0 (Thursday 11th November 2021)

 - The `-rpath` is explicitly set on executables and shared libraries, and the
   `-install_name` set on shared libraries under macOS. This is to support
   local development, and execution of commands from their `$FSLDIR/src/`
   directory.


## 2109.1 (Wednesday 22nd September 2021)

 - Updated the `supportedGencodes.sh` script for CUDA 11.2, 11.3, and 11.4.


## 2109.0 (Monday 13th September 2021)

 - Added an extra unit test.


## 2108.3 (Friday 13th August 2021)

 - Fix to the `Makefile`, as `$FSLDIR/doc/fsl.css` was not being installed.


## 2108.2 (Wednesday 11th August 2021)

 - Change to `createFSLWrapper` and `removeFSLWrapper` to accommodate
   naming conventions for FSL GUI commands (`<Command>_gui` on macOS,
   and `<Command>` on Linux).


## 2108.1 (Monday 9th August 2021)

 - Reverted the python library installation directory to `$FSLDIR/python` -
   this can be changed in the future if it is ever deemed problematic.


## 2108.0 (Friday 6th August 2021)


 - Added some files used by FEAT and MELODIC to generate HTML reports.
 - The `$FSLDIR/etc/fslconf/fsl.sh` script now uses `$FSLDIR/etc/fslversion`
   to identify official FSL installations - if this file is present,
   `$FSLDIR/share/fsl/bin` is automatically added to the `$PATH` variable.
 - The `$FSLDIR/share/fsl/sbin/createFSLWrapper` script now detects `pythonw`
   executables.


## 2107.3 (Monday 12th July 2021)

 - Change CUDA build configuration so that now all is needed is for `nvcc` to
   be on the `$PATH`, or a `$NVCC` variable to be set.


## 2107.2 (Sunday 11th July 2021)

 - Further tweaks to CUDA linking logic.


## 2107.1 (Friday 9th July 2021)

 - Fix an issue with CUDA linking.


## 2107.0 (Thursday 8th July 2021)

 - Allow CUDA projects to statically link to the CUDA runtime and
   toolkit components, by setting a variable `CUDA_STATIC`.


## 2106.2 (Tuesday 22nd June 2021)

 - Fixed an issue with `createFSLWrapper` on macOS.


## 2106.1 (Friday 6th June 2021)

 - Updated the `supportedGencodes.sh` script to add JIT targets for CUDA
   projects.
 - Added a Makefile.


## 2101.5 (Saturday 23rd January 2021)

 - Updated the `supportedGencodes.sh` script to support CUDA versions
   10.2, 11.1 and 11.2.
 - Fixed a bug in the final creation of `NVCCFLAGS` and `NVCCLDFLAGS`.


## 2101.4 (Friday 22nd January 2021)

 - Fixed a bug in the default CFLAGS assignment under Linux.
 - Cleaned up and adjusted support for NVCC-compiled CUDA projects to work
   better within a conda build environment. May still be in a state of flux.


## 2101.3 (Thursday 14th January 2021)

 - The `$FSLDIR/etc/fslconf/fsl.sh` script now automatically adds
   `$FSLDIR/share/fsl/bin/` to the `$PATH`, for official
   (`fslinstaller`-installed) FSL installations.
 - The `$FSLDIR/etc/fslconf/fsl-devel.sh` script now activates the `$FSLDIR`
   conda environment, for official (`fslinstaller`-installed) FSL
   installations.
 - The `$FSLDIR/etc/fslconf/fsl-devel.sh` script now assigns default values
   to `$FSLDEVDIR` and `$FSLCONFDIR`, if they are not set by the user.


## 2101.2 (Friday 8th January 2021)

 - Removed the legacy `bin/fsl_sub` script - `fsl-sub` is now available on
   conda, so can be listed as a dependency of the `fsl/base` project.


## 2101.1 (Tuesday 5th January 2021)

 - Move contents of `config/common/` into `config/` - there is no need for
   a separate directory level now that we aren't maintaining separate
   configurations for different architectures/compilers.
 - The default C++ compilation standard is now `-std=c++11` (previously
   it was `-std=c++98`/`-ansi`).
 - Allow projects to set custom C preprocessor flags, via `USRCPPFLAGS`.


## 2101.0 (Monday 4th January 2021)

 - Moved the copyright insertion machinery (script and templates) from
   `config/common/` into `share/fsl/`.
 - Unify `fsl.sh` and `fsl-devel.sh` so the latter simply invokes the former.
 - Fix an issue with argument pass-through in the `fslpython[w]` wrapper
   scripts.
 - Removed the `etc/fslconf/fslmachtype.sh` script, as neither it, nor the
   `FSLMACHTYPE` environment variable, are used by anything.


## 2012.0 (Wednesday 2nd December 2020)

 - Added `creeateFSLWrapper` and `removeFSLWrapper`, utility commands, which
   will be used to create isolated wrapper scripts of all FSL executables in a
   separate sub-directory, so they can be added to the user `$PATH` without
   other executables in `$PREFIX/bin/` causing conflicts (!1).


## 2011.0 (Monday 2nd November 2020)

 - Initial release of the FSL `base` project - an amalagamation of `fsl/etc`
   and `fsl/config`, which has been adjusted to compile shared libraries with
   dynamic linking, and tweaked for use as the base of a `conda`-managed FSL
   installation.
