#########################################################
# Variables used for FSL project compilation/installation
#
# Default compiler/platform specific variables are
# initialised in buildSettings.mk, which is included
# before this file. Project specific variables are
# initialised in the project Makefile.
#########################################################

# Boolean flag which defines this FSL installation
# as being from FSL 6.0.6 or newer ("GE" = greater
# than or equal to). This flag may be used by FSL
# Makefiles which need to preserve support for
# older versions of FSL.
FSL_GE_606 := true

# All projects must specify a name
PROJNAME ?=

# Project-specific compiler/linker flags - these
# can be overridden in the project Makefile, or
# by the environment.

USRLDFLAGS     ?= # Linker flags
USRINCFLAGS    ?= # Include directories
USRCFLAGS      ?= # Compiler flags for C files
USRCXXFLAGS    ?= # Compiler flags for C++ files
USRCPPFLAGS    ?= # Preprocessor flags
USRNVCCFLAGS   ?= # Compiler flags for CUDA files
USRNVCCLDFLAGS ?= # Linker flags for CUDA libraries/executables

# Libraries to link against for C and C++ projects -
# these are incorporated into the final LDFLAGS, below.
LIBS ?=

# CUDA libraries to link against (e.g. curand, cublas, etc) -
# these are incorporated into the final NVCCLDFLAGS, below.
# -lcuda and -lcudart are automatically added, so do not
# need to be explicitly listed. Dynamic library names should
# be used here - they will be transformed to their static
# versions if static compilation is selected is set (see
# buildSettings.mk).
CUDALIBS ?=

# Project outputs - these variables control
# what gets installed from a project. They
# may be customised/overridden in project
# Makefiles.
HFILES       ?= *.h    # installed into $FSLDIR/include/$PROJNAME/
GLOBALHFILES ?=        # installed into $FSLDIR/include/ (should
                       # not be used in normal circumstances)
AFILES       ?= *.a    # installed into $FSLDIR/lib/
SOFILES      ?= *.so   # installed into $FSLDIR/lib/
XFILES       ?=        # installed into $FSLDIR/bin/
FXFILES      ?=        # installed into $FSLDIR/bin/, only for internal builds
SCRIPTS      ?=        # installed into $FSLDIR/bin/
FSCRIPTS     ?=        # installed into $FSLDIR/bin/, only for internal builds
RUNTCLS      ?=        # link to Runtcl created in $FSLDIR/bin/ which assumes
                      # that <file>.tcl has been installed into
                      # $FSLDIR/tcl/
PYFILES      ?= *.py   # installed into $FSLDIR/etc/fsl/python/$PROJNAME/
TCLFILES     ?= *.tcl  # installed into $FSLDIR/tcl/
DATAFILES    ?=        # installed into $FSLDIR/data/$PROJNAME/
TESTXILES    ?=        # not currently used

# Final install destinations
# for project outputs
DESTDIR      = ${FSLDEVDIR}
dest_INCDIR  = ${DESTDIR}/include
dest_LIBDIR  = ${DESTDIR}/lib
dest_BINDIR  = ${DESTDIR}/bin
dest_TCLDIR  = ${DESTDIR}/tcl
dest_PYDIR   = ${DESTDIR}/python
dest_DOCDIR  = ${DESTDIR}/doc
dest_DATADIR = ${DESTDIR}/data

# Standard header/library locations for compiling/linking
FSL_INCDIR    = ${FSLDIR}/include
FSL_LIBDIR    = ${FSLDIR}/lib
FSL_DEVINCDIR = ${FSLDEVDIR}/include
FSL_DEVLIBDIR = ${FSLDEVDIR}/lib

# Final header include directories
INCFLAGS     = -isystem ${FSL_DEVINCDIR} -isystem ${FSL_INCDIR} -I. ${USRINCFLAGS}
NVCCINCFLAGS = ${INCFLAGS} \
               -isystem ${CUDA_HOME}/include \
               -isystem ${CUDA_HOME}/targets/x86_64-linux/include

#############################
# Final compiler/linker flags
#############################

# All projects must use these flags for compilation/linking.
# Commands for compilation of intermediate object files
# should have the form:

#     $(CC)   $(CFLAGS)    <input/output files> # for .c files
#     $(CXX)  $(CXXFLAGS)  <input/output files> # for .cc files
#     $(NVCC) $(NVCCFLAGS) <input/output files> # for .cu files
#
# And commands for compilation and linking of executables
# and libraries should have the form:
#
#     $(CC)   $(CFLAGS)            <input/output files> ${LDFLAGS}     # for c libs/exes
#     $(CXX)  $(CXXFLAGS)          <input/output files> ${LDFLAGS}     # for c++ exes
#     $(CXX)  $(CXXFLAGS)  -shared <input/output files> ${LDFLAGS}     # for c++ libs
#     $(NVCC) $(NVCCFLAGS) -shared <input/output files> ${NVCCLDFLAGS} # for CUDA libs
#     $(NVCC) $(NVCCFLAGS)         <input/output files> ${NVCCLDFLAGS} # for CUDA exes
#
# `LDFLAGS` *must* come at the end, to ensure proper linking.
#
# The order in which the final FLAGS variables are
# constructed here is important:
#
#  1. Include flags come first, so they aren't
#     overridden by the environment.
#  2. Flags provided by the environment come next
#  3. ARCH* flags defined in buildSettings.mk come next
#  4. USR* flags defined in the Project Makefile come
#     last.
#
# Many compiler options, if specified multiple times,
# are interpreted such that only the last option takes
# effect. This essentiallyy means that, because of the
# construction order, a project Makefile can override
# the default options specified in buildSettings.mk,
# which in turn can override options provided by the
# environment.
#
# We save copis of *FLAGS from the environment before
# constructing their final values, so we can prepend
# flags before them without running into issues w.r.t.
# recursively expanded variables.
_ENVCFLAGS   := ${CFLAGS}
_ENVCPPFLAGS := ${CPPFLAGS}
_ENVCXXFLAGS := ${CXXFLAGS}
_ENVLDFLAGS  := ${LDFLAGS}

# Final construction of compiler flags
CPPFLAGS = ${_ENVCPPFLAGS} ${ARCHCPPFLAGS} ${USRCPPFLAGS}
CFLAGS   = ${INCFLAGS} ${_ENVCFLAGS}   ${CPPFLAGS} ${ARCHCFLAGS}   ${USRCFLAGS}
CXXFLAGS = ${INCFLAGS} ${_ENVCXXFLAGS} ${CPPFLAGS} ${ARCHCXXFLAGS} ${USRCXXFLAGS}
LDFLAGS  = ${_ENVLDFLAGS} ${ARCHLDFLAGS} ${USRLDFLAGS} \
           -L. -L${FSL_DEVLIBDIR} -L${FSL_LIBDIR}  \
            ${LIBS} ${ARCHLIBS}

# CUDACXXFLAGS is used for compiling .cpp files with
# g++ which include CUDA headers. This really shouldn't
# be neeed, but is used by eddy for the time being.
CUDACXXFLAGS = ${NVCCINCFLAGS} ${CXXFLAGS} ${USRNVCCFLAGS}

# Remove any -std=c++ options, as we are already setting
# -std in ARCHNVCCFLAGS (seee buildSettings.mk), and
# passing another one via --compiler-options will confuse
# nvcc. We assume that project Makefile has not added
# -std to USRCXXFLAGGS/USRCPPFLAGS.
NVCCFLAGS   += ${NVCCINCFLAGS}                                             \
               --compiler-options "$(patsubst -std=c++%,,${_ENVCXXFLAGS})" \
               --compiler-options "${CPPFLAGS}"                            \
               ${ARCHNVCCFLAGS}                                            \
               --compiler-options "$(patsubst -std=c++%,,${ARCHCXXFLAGS})" \
               ${USRNVCCFLAGS}                                             \
               --compiler-options "${USRCXXFLAGS}"

NVCCLDFLAGS += ${ARCHNVCCLDFLAGS}                     \
               --compiler-options "${ARCHLDFLAGS}"    \
               ${USRNVCCLDFLAGS}                      \
               --compiler-options "${USRLDFLAGS}"     \
               -L. -L${FSL_DEVLIBDIR} -L${FSL_LIBDIR} \
                ${LIBS} ${_CUDALIBS} ${ARCHLIBS}
