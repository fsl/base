# Platform specific settings and defaults,
# which are used by vars.mk
include ${FSLCONFDIR}/buildSettings.mk

# Definition of variables controlling
# compilation and installation
include ${FSLCONFDIR}/vars.mk

# Make rules
include ${FSLCONFDIR}/rules.mk
