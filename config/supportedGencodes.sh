#!/bin/sh
#
# Returns a string with allowed -gencode value pairs
# for the given CUDA version.
#
# Compute capabilities that are supported by each CUDA
# version are listed in the "Compute Capabilities"
# table of the Programming guide for each CUDA version,
# e.g.:
#
# https://docs.nvidia.com/cuda/archive/11.4.1/cuda-c-programming-guide/index.html#compute-capabilities
#
# The topic of binary and PTX compute capability
# compatibility is described in the CUDA programming
# guide:
#
# https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#binary-compatibility


vn=$1

if [ "$vn" = 5.5 ]
then
    echo "-gencode arch=compute_10,code=sm_10 \
          -gencode arch=compute_11,code=sm_11 \
          -gencode arch=compute_12,code=sm_12 \
          -gencode arch=compute_13,code=sm_13 \
          -gencode arch=compute_20,code=sm_20 \
          -gencode arch=compute_20,code=sm_21 \
          -gencode arch=compute_30,code=sm_30 \
          -gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_35,code=compute_35"
elif [ "$vn" = 6.0 ]
then
    echo "-gencode arch=compute_10,code=sm_10 \
          -gencode arch=compute_11,code=sm_11 \
          -gencode arch=compute_12,code=sm_12 \
          -gencode arch=compute_13,code=sm_13 \
          -gencode arch=compute_20,code=sm_20 \
          -gencode arch=compute_20,code=sm_21 \
          -gencode arch=compute_30,code=sm_30 \
          -gencode arch=compute_32,code=sm_32 \
          -gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_50,code=compute_50"
elif [ "$vn" = 6.5 ]
then
    echo "-gencode arch=compute_11,code=sm_11 \
          -gencode arch=compute_12,code=sm_12 \
          -gencode arch=compute_13,code=sm_13 \
          -gencode arch=compute_20,code=sm_20 \
          -gencode arch=compute_20,code=sm_21 \
          -gencode arch=compute_30,code=sm_30 \
          -gencode arch=compute_32,code=sm_32 \
          -gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_37,code=sm_37 \
          -gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_50,code=compute_50"
elif [ "$vn" = 7.0 ] ||
     [ "$vn" = 7.5 ]
then
    echo "-gencode arch=compute_20,code=sm_20 \
          -gencode arch=compute_20,code=sm_21 \
          -gencode arch=compute_30,code=sm_30 \
          -gencode arch=compute_32,code=sm_32 \
          -gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_37,code=sm_37 \
          -gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_52,code=sm_52 \
          -gencode arch=compute_53,code=sm_53 \
          -gencode arch=compute_53,code=compute_53"
elif [ "$vn" = 8.0 ]
then
    echo "-gencode arch=compute_20,code=sm_20 \
          -gencode arch=compute_20,code=sm_21 \
          -gencode arch=compute_30,code=sm_30 \
          -gencode arch=compute_32,code=sm_32 \
          -gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_37,code=sm_37 \
          -gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_52,code=sm_52 \
          -gencode arch=compute_53,code=sm_53 \
          -gencode arch=compute_60,code=sm_60 \
          -gencode arch=compute_61,code=sm_61 \
          -gencode arch=compute_62,code=sm_62 \
          -gencode arch=compute_62,code=compute_62"
elif [ "$vn" = 9.0 ] ||
     [ "$vn" = 9.1 ]
then
    echo "-gencode arch=compute_30,code=sm_30 \
          -gencode arch=compute_32,code=sm_32 \
          -gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_37,code=sm_37 \
          -gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_52,code=sm_52 \
          -gencode arch=compute_53,code=sm_53 \
          -gencode arch=compute_60,code=sm_60 \
          -gencode arch=compute_61,code=sm_61 \
          -gencode arch=compute_62,code=sm_62 \
          -gencode arch=compute_70,code=sm_70 \
          -gencode arch=compute_70,code=compute_70"
elif [ "$vn" = 9.2 ]
then
    echo "-gencode arch=compute_30,code=sm_30 \
          -gencode arch=compute_32,code=sm_32 \
          -gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_37,code=sm_37 \
          -gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_52,code=sm_52 \
          -gencode arch=compute_53,code=sm_53 \
          -gencode arch=compute_60,code=sm_60 \
          -gencode arch=compute_61,code=sm_61 \
          -gencode arch=compute_62,code=sm_62 \
          -gencode arch=compute_70,code=sm_70 \
          -gencode arch=compute_72,code=sm_72 \
          -gencode arch=compute_72,code=compute_72"
elif [ "$vn" = 10.0 ] ||
     [ "$vn" = 10.1 ] ||
     [ "$vn" = 10.2 ]
then
    echo "-gencode arch=compute_30,code=sm_30 \
          -gencode arch=compute_32,code=sm_32 \
          -gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_37,code=sm_37 \
          -gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_52,code=sm_52 \
          -gencode arch=compute_53,code=sm_53 \
          -gencode arch=compute_60,code=sm_60 \
          -gencode arch=compute_61,code=sm_61 \
          -gencode arch=compute_62,code=sm_62 \
          -gencode arch=compute_70,code=sm_70 \
          -gencode arch=compute_72,code=sm_72 \
          -gencode arch=compute_75,code=sm_75 \
          -gencode arch=compute_75,code=compute_75"
elif [ "$vn" = 11.0 ]
then
    echo "-gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_37,code=sm_37 \
          -gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_52,code=sm_52 \
          -gencode arch=compute_53,code=sm_53 \
          -gencode arch=compute_60,code=sm_60 \
          -gencode arch=compute_61,code=sm_61 \
          -gencode arch=compute_62,code=sm_62 \
          -gencode arch=compute_70,code=sm_70 \
          -gencode arch=compute_72,code=sm_72 \
          -gencode arch=compute_75,code=sm_75 \
          -gencode arch=compute_80,code=sm_80 \
          -gencode arch=compute_80,code=compute_80"
elif [ "$vn" = 11.1 ] ||
     [ "$vn" = 11.2 ] ||
     [ "$vn" = 11.3 ] ||
     [ "$vn" = 11.4 ]
then
    echo "-gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_37,code=sm_37 \
          -gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_52,code=sm_52 \
          -gencode arch=compute_53,code=sm_53 \
          -gencode arch=compute_60,code=sm_60 \
          -gencode arch=compute_61,code=sm_61 \
          -gencode arch=compute_62,code=sm_62 \
          -gencode arch=compute_70,code=sm_70 \
          -gencode arch=compute_72,code=sm_72 \
          -gencode arch=compute_75,code=sm_75 \
          -gencode arch=compute_80,code=sm_80 \
          -gencode arch=compute_86,code=sm_86 \
          -gencode arch=compute_86,code=compute_86"
elif [ "$vn" = 11.5 ] ||
     [ "$vn" = 11.6 ] ||
     [ "$vn" = 11.7 ]
then
    echo "-gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_37,code=sm_37 \
          -gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_52,code=sm_52 \
          -gencode arch=compute_53,code=sm_53 \
          -gencode arch=compute_60,code=sm_60 \
          -gencode arch=compute_61,code=sm_61 \
          -gencode arch=compute_62,code=sm_62 \
          -gencode arch=compute_70,code=sm_70 \
          -gencode arch=compute_72,code=sm_72 \
          -gencode arch=compute_75,code=sm_75 \
          -gencode arch=compute_80,code=sm_80 \
          -gencode arch=compute_86,code=sm_86 \
          -gencode arch=compute_87,code=sm_87 \
          -gencode arch=compute_87,code=compute_87"
elif [ "$vn" = 11.8 ]
then
    echo "-gencode arch=compute_35,code=sm_35 \
          -gencode arch=compute_37,code=sm_37 \
          -gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_52,code=sm_52 \
          -gencode arch=compute_53,code=sm_53 \
          -gencode arch=compute_60,code=sm_60 \
          -gencode arch=compute_61,code=sm_61 \
          -gencode arch=compute_62,code=sm_62 \
          -gencode arch=compute_70,code=sm_70 \
          -gencode arch=compute_72,code=sm_72 \
          -gencode arch=compute_75,code=sm_75 \
          -gencode arch=compute_80,code=sm_80 \
          -gencode arch=compute_86,code=sm_86 \
          -gencode arch=compute_87,code=sm_87 \
          -gencode arch=compute_89,code=sm_89 \
          -gencode arch=compute_90,code=sm_90 \
          -gencode arch=compute_90,code=compute_90"
elif [ "$vn" = 12.0 ] ||
     [ "$vn" = 12.1 ] ||
     [ "$vn" = 12.2 ] ||
     [ "$vn" = 12.3 ] ||
     [ "$vn" = 12.4 ] ||
     [ "$vn" = 12.5 ] ||
     [ "$vn" = 12.6 ]
then
    echo "-gencode arch=compute_50,code=sm_50 \
          -gencode arch=compute_52,code=sm_52 \
          -gencode arch=compute_53,code=sm_53 \
          -gencode arch=compute_60,code=sm_60 \
          -gencode arch=compute_61,code=sm_61 \
          -gencode arch=compute_62,code=sm_62 \
          -gencode arch=compute_70,code=sm_70 \
          -gencode arch=compute_72,code=sm_72 \
          -gencode arch=compute_75,code=sm_75 \
          -gencode arch=compute_80,code=sm_80 \
          -gencode arch=compute_86,code=sm_86 \
          -gencode arch=compute_87,code=sm_87 \
          -gencode arch=compute_89,code=sm_89 \
          -gencode arch=compute_90,code=sm_90 \
          -gencode arch=compute_90,code=compute_90"
else
    echo "Unrecognised CUDA version: $vn"
    exit 1
fi
