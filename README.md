# fsl-base

This project contains the "base" of an FSL installation. It includes:

 - Configuration scripts for setting up an environment to use FSL
 - Makefile infrastructure used to compile and install C++ FSL projects
 - Various other small configuration, data, and auxillary files

Prior to FSL 6.0.6, the contents of this project were provided by the
[etc](https://git.fmrib.ox.ac.uk/fsl/etc) and
[config](https://git.fmrib.ox.ac.uk/fsl/config) projects.


Most of the contents of the `base` project are copied directly into `$FSLDIR`.
Some Python scripts and modules, contained in the `python` sub-directory,
are installed as a Python library.


## fsl-base release management


When preparing a new version of fsl-base, make sure that:
 - The version in `python/setup.py` has been updated
 - The change log has been updated.

Then:
 - create a new tag on the fsl/base> gitlab repository, and
 - manage the conda package build at the fsl/conda/fsl-base> repository.



## Dependencies

Dependencies of the `base` project are specified in the fsl/conda/fsl-base>
conda recipe.


## Tests

The `tests` directory contains a collection of tests which validate the
behaviour of various aspects of the `base` project. Note that some of the
tests must be executed from a conda environment with all dependencies
installed. The tests are automaticaly run on the fsl/conda/fsl-base> conda
recipe repository, when a new `fsl-base` conda package is built.


## The FSL `Makefile` system

The `base` project provides GNU Make infrastructure for compiling and
installing projects. FSL projects which provide executables or libraries
written in C, C++, CUDA, Shell, or TCL should use this `Makefile` system.
When writing a `Makefile` for a new project, the best option is to look
at and copy the existing `Makefile` for a similar project.


## Conventions for C/C++/CUDA projects

An example `Makefile` for a C++ project which provides executables `myexe1`
and `myexe2`, and a shared library `libmylib.so` might look like this:

```
include ${FSLCONFDIR}/default.mk

PROJNAME = myproject
SOFILES  = libmylib.so   # SOFILES will be installed into $FSLDIR/lib/
XFILES   = myexe1 myexe2 # XFILES will be installed into $FSLDIR/bin/

# There is no need to define a rule to compile
# .o files - default rules are defined in
# $FSLDIR/config/rules.mk

libmylib.so: myobj1.o myobj2.o
    $(CXX) $(CXXFLAGS) -shared -o $@ $^ $(LDFLAGS)

myexe1: myexe1.o myobj1.o
    $(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

myexe2: myexe2.o myobj2.o
    $(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
```


Commands for compilation of intermediate object files should have the
form (note that these rules are already defined, so there is usually
no need to re-define them):

    $(CC)   $(CFLAGS)    <input/output files> # for .c files
    $(CXX)  $(CXXFLAGS)  <input/output files> # for .cc files
    $(NVCC) $(NVCCFLAGS) <input/output files> # for .cu files

And commands for compilation and linking of executables and libraries
should have the form:

    $(CC)   $(CFLAGS)            <input/output files> ${LDFLAGS}     # for c libs/exes
    $(CXX)  $(CXXFLAGS)          <input/output files> ${LDFLAGS}     # for c++ exes
    $(CXX)  $(CXXFLAGS)  -shared <input/output files> ${LDFLAGS}     # for c++ libs
    $(NVCC) $(NVCCFLAGS) -shared <input/output files> ${NVCCLDFLAGS} # for CUDA libs
    $(NVCC) $(NVCCFLAGS)         <input/output files> ${NVCCLDFLAGS} # for CUDA exes


`LDFLAGS` *must* come at the end, to ensure proper linking.


## Conventions for Python projects

Simple stand-alone python scripts can be installed directly into
`$FSLDIR/bin/`, with a `Makefile` like so:

```
include ${FSLCONFDIR}/default.mk

PROJNAME = myproject
SCRIPTS  = myscript  # SCRIPTS will be installed into $FSLDIR/bin/
```

The hash-bang line within your Python script (`myscript` in the above example)
**must** be `#/usr/bin/env fslpython` - this will ensure that the correct
Python interpreter is used when your script is executed.


Python projects which provide importable modules must be installed through
pip, and so must provide a `setup.py` file. If the project is "pure" Python,
i.e. no other C/C++ executables or shell scripts, a `Makefile` is not
necessary - the project will be built by its corresponding conda recipe. For
"hybrid" projects, which provide both Python and C++ components, the
`Makefile` should contain a `pyinstall` rule which calls `pip`, e.g.:


```
include ${FSLCONFDIR}/default.mk

PROJNAME = myproject
XFILES   = mycxxexe

pyinstall:
    python -m pip install . --prefix ${FSLDEVDIR} --no-deps --ignore-installed --no-cache-dir -vvv

mycxxexe:
    $(CXX) $(CXXFLAGS) -o $@ mycxxexe.cpp $(LDFLAGS)
```

Dependencies of all FSL projects are managed in the FSL conda recipes (see
the fsl/conda> gitlab namespace), so the `--no-deps` option must be provided.


## Building FSL CUDA projects

Some FSL projects (e.g. [`fsl/eddy`(https://git.fmrib.ox.ac.uk/fsl/eddy) use
CUDA for GPU acceleration. In order to compile these projects, the `nvcc`
compiler must be available on your `$PATH`, or a variable called `$NVCC` must
refer to the `nvcc` executable to use.

The following additional options may be used to control compilation of a CUDA
project:

 - `CUDA_DYNAMIC`: Dynamically link against the CUDA runtime and certain;
                   CUDA Toolkit libraris (default is to use static linking).
 - `GENCODEFLAGS`: `-gencode` options specifying the device architectures to
                   compile device code for (default is to use the
                   `config/supportedGencodes.sh` script).

For example, to compile a CUDA project with a specific CUDA installation, and
using dynamic linking:

    export PATH=/usr/local/cuda-10.2/bin:$PATH
    make CUDA_DYNAMIC=1


The `Makefile` for each FSL CUDA project may provide additional options for
controlling compilation.

An example `Makefile` for a C++/CUDA project might look like this:

```
include ${FSLCONFDIR}/default.mk

PROJNAME = myproject
XFILES   = myexe

# There is no need to define a rule to compile
# .o files - default rules are defined in
# $FSLDIR/config/rules.mk

myexe:
    $(NVCC) $(NVCCFLAGS) -o $@ $< $(NVCCLDFLAGS)
```
